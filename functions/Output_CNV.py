
import os
from utils import *

def Output_CNV(reads, FASTA, output, output_name, NAME, PLATFORM):

	
	totalPos = float(GetTotalPos2(reads["R1"])*2)
	currentPos = 1.0
	lastProgress = 0.0

	print("\n")
	PrintTime("console", "\tGenerating FASTQ...")

	# => fastq_R1
	output_R1 = open(output+"/"+output_name+"_R1.fastq", 'w')

	for x in reads["R1"].keys():
		y = x.split("-")
		chrom = y[0]
		start = int(y[1])
		end   = int(y[2])


		for read_id, read in reads["R1"][x].items():

			lastProgress = PrintProgress(currentPos, totalPos, lastProgress)

			readArray = read.split('|')
			read = readArray[0].upper()
			read_qual = readArray[1]



			line = read_id+"\n"+read+"\n"+"+\n"+read_qual+"\n"
			output_R1.write(line)

			currentPos += 1.0

	output_R1.close()





	# => fastq_R2
	output_R2 = open(output+"/"+output_name+"_R2.fastq", 'w')

	for x in reads["R2"].keys():
		y = x.split("-")
		chrom = y[0]
		start = int(y[1])
		end   = int(y[2])

		for read_id, read in reads["R2"][x].items():

			lastProgress = PrintProgress(currentPos, totalPos, lastProgress)

			readArray = read.split('|')
			read = readArray[0].upper()

		
			read_qual = readArray[1]
			

			read = ReverseComplement(read)
			read_qual = read_qual[::-1]
			line = read_id+"\n"+read+"\n"+"+\n"+read_qual+"\n"
			output_R2.write(line)

			currentPos += 1.0

	output_R2.close()






	print("\n")
	PrintTime("console", "\tDone")
	print("\n")


	# bwa => sam
	PrintTime("console", "\tConverting FASTQ to SAM")
	os.system('bwa mem -t 10 -R "@RG\tID:'+NAME+'\tPL:'+PLATFORM+'\tSM:'+NAME+'" '+FASTA+' '+output+"/"+output_name+'_R1.fastq '+output+"/"+output_name+'_R2.fastq > '+output+"/"+output_name+'.sam')
	PrintTime('console', '\tDone')
	print("\n")

	# samtools => bam
	PrintTime("console", "\tConverting SAM to BAM...")
	os.system('samtools view -Sb '+output+"/"+output_name+'.sam > '+output+"/"+output_name+'.bam')
	PrintTime('console', '\tDone')
	print("\n")

	# samtools => sorted_bam
	PrintTime("console", "\tSorting BAM...")
	os.system('samtools sort '+output+"/"+output_name+'.bam > '+output+"/"+output_name+'_sorted.bam')
	os.remove(output+"/"+output_name+".bam")
	os.rename(output+"/"+output_name+"_sorted.bam", output+"/"+output_name+".bam")
	PrintTime('console', '\tDone')
	print("\n")

	# samtools => index
	PrintTime("console", "\tIndexing BAM...")
	os.system('samtools index '+output+"/"+output_name+'.bam')
	PrintTime('console', '\tDone')