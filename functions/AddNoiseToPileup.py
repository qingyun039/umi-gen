#
# THIS FUNCTION ALLOWS TO ADD ARTIFACTS AS NOISE TO THE PILEUP
#
# INPUT : -PILEUP            : (DICT) THE DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
# 		  -VARIANTS          : (LIST) THE LIST OF VARIANTS FOUND BY UMI-VARCAL FOUND IN THE CONTROL SAMPLES
# 		  -QSCORES           : (DICT) A DIXTIONNARY CONTAINING THE LIST OF QUALITY SCORES FOUND FOR EACH POSITION OF THE PILEUP
#
# VALUE : -PILEUP            : (DICT) THE UPDATED DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC

from utils import *

def AddNoiseToPileup(pileup, variants, qScores):
	# loop through different positions of the pileup
	for chrom in pileup.keys():
		for pos in pileup[chrom].keys():
			# calculate median quality score and add it to the pileup
			pileup[chrom][pos]['qScore'] = median(qScores[chrom][pos])
			# calculate the base error probability from the quality score
			base_error_probability = 10**(float(pileup[chrom][pos]['qScore'])/-10)
			# add the base error probability to the pileup
			pileup[chrom][pos]['base_error_probability'] = round(base_error_probability, 6)

			# build the index to see if position is in the list of variants
			index = chrom+"-"+str(pos)

			# if the index is in variants, change to 0 all probabilities and then make the reference at 1 (0.5/0.5)
			if index in variants:
				pileup[chrom][pos]['A'][0] = 0.0  
				pileup[chrom][pos]['A'][1] = 0.0  
				pileup[chrom][pos]['C'][0] = 0.0  
				pileup[chrom][pos]['C'][1] = 0.0  
				pileup[chrom][pos]['G'][0] = 0.0  
				pileup[chrom][pos]['G'][1] = 0.0  
				pileup[chrom][pos]['T'][0] = 0.0  
				pileup[chrom][pos]['T'][1] = 0.0 

				pileup[chrom][pos][pileup[chrom][pos]['ref']][0] = 0.5  
				pileup[chrom][pos][pileup[chrom][pos]['ref']][1] = 0.5 

				pileup[chrom][pos]['del'] = {}
				pileup[chrom][pos]['in'] = {}

			# if not in variants
			else:

				# change the counts of each base to probabilities by dividing by the depth of each position
				depth = float(pileup[chrom][pos]['depth'])
				try:
					pileup[chrom][pos]['A'][0] /= depth  
					pileup[chrom][pos]['A'][1] /= depth  
					pileup[chrom][pos]['C'][0] /= depth  
					pileup[chrom][pos]['C'][1] /= depth  
					pileup[chrom][pos]['G'][0] /= depth  
					pileup[chrom][pos]['G'][1] /= depth  
					pileup[chrom][pos]['T'][0] /= depth  
					pileup[chrom][pos]['T'][1] /= depth  
					pileup[chrom][pos]['del'] = {}
					pileup[chrom][pos]['in'] = {}
				except:
					pass


	# return pileup
	return pileup