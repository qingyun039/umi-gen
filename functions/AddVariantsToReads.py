#
# THIS FUNCTION ALLOWS TO ADD TRUE VARIANTS TO THE READS
#
# INPUT : -PILEUP            : (DICT) THE DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
#         -READS             : (DICT) A DICTIONNARY CONTAINING ALL OF R1 AND R2 REFERENCE READS
# 		  -VARIANTS          : (LIST) THE LIST OF VARIANTS TO BE ADDED TO THE READS
#         -USEDUMIS          : (LIST) A LIST OF ALL UMIS THAT ARE ALREADY USED ADN PRESENT AS REFERENCE UMIS
# 		  -BED               : (STR) LOCATION OF THE BED FILE
#         -DEPTH             : (INT) DESIRED DEPTH OF THE FINAL SAMPLE
#         -AMP_FACTOR        : (INT) THE AMPLFICATION FACTOR OF EACH UMI 
#         -UMI_LENGTH	     : (INT) DESIRED LENGTH OF THE UMI SEQUENCE
#         -READ_LENGTH       : (INT) LENGTH OF THE GENERATED READS
#
# VALUE : -PILEUP            : (DICT) THE UPDATED DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
#  	 	  -INSERTIONS        : (DICT) DICTIONNARY CONTAINING ALL THE READS HAVING A SPECIFIC INSERTION
#         -DELETIONS         : (DICT) DICTIONNARY CONTAINING ALL THE READS HAVING A SPECIFIC DELETION

from utils import *
import random

def AddVariantsToReads(pileup, reads, variants, usedUMIS, BED, DEPTH, UMI_LENGTH, READ_LENGTH):

	print("\n")
	PrintTime('console', '\tAdding Variants...')

	insertions = {}
	deletions = {}

	currentPos = 1.0
	lastProgress = 0.0
	totalPos = float(len(variants))

	ranges_d = ParseRanges(BED, READ_LENGTH)

	lenToAlpha = {
		"del": {1: "b",2: "d",3: "e",4: "f",5: "h",6: "i",7: "j",8: "k",9: "l",10: "m"}, 
		"in" : {1: "n",2: "o",3: "p",4: "q",5: "r",6: "s",7: "u",8: "v",9: "w",10: "x"}
	}

	for index, infos in variants.items():

		# print index

		lastProgress = PrintProgress(currentPos, totalPos, lastProgress)

		index = index.split(':')
		chrom = index[0]
		pos = int(index[1])

		freq = infos[1]
		
		typee = infos[2]
		if typee == "sub": 
			alt = infos[0]
		elif typee == "in":
			ref = infos[0][0]
			alt = infos[0][1:]
			insertions[chrom+":"+str(pos)] = [ref, alt]
			alt = lenToAlpha[typee][len(alt)]
		else:
			ref = infos[0][1:]
			alt = infos[0][0]
			deletions[chrom+":"+str(pos)] = [ref, alt]
			alt = lenToAlpha[typee][len(ref)]
		
		varRange = GetRange(ranges_d[chrom], pos)
		var_reads = int(round(freq*DEPTH/2, 0))

		start = varRange[0]
		end   = varRange[1]
		cursor = pos - start

		INDEX = chrom+"-"+str(start)+"-"+str(end)

		readsIDS1 = reads['R1'][INDEX].keys()
		readsIDS2 = reads['R2'][INDEX].keys()

		while var_reads > 0:

			raw = True
			while raw == True:

				chosenKey1 = random.choice(list(readsIDS1))
				chosenKey2 = chosenKey1.replace("1:N:0:1", "2:N:0:1")
				
				chosenRead1 = list(reads['R1'][INDEX][chosenKey1])
				chosenRead2 = list(reads['R2'][INDEX][chosenKey2])


				if chosenRead1[cursor].isupper() and chosenRead2[cursor].isupper():

					chosenRead1[cursor] = alt.lower()
					chosenRead2[cursor] = alt.lower()

					new_umi = False
					while new_umi == False:
						umi = GenerateUMI(UMI_LENGTH)
						if umi not in usedUMIS:

							old_umi = chosenKey1.split(' ')[0].split('_')[1]
							newKey1 = chosenKey1.replace(old_umi, umi)
							newKey2 = chosenKey2.replace(old_umi, umi)

							del reads['R1'][INDEX][chosenKey1]
							del reads['R2'][INDEX][chosenKey2]

							reads['R1'][INDEX][newKey1] = "".join(chosenRead1)
							reads['R2'][INDEX][newKey2] = "".join(chosenRead2)

							readsIDS1 = reads['R1'][INDEX].keys()
							readsIDS2 = reads['R2'][INDEX].keys()
							
							usedUMIS.add(umi)
							new_umi = True


					raw = False
					var_reads -= 1


		currentPos += 1.0


		

	print("\n")
	PrintTime('console', '\tDone')

	return [reads, insertions, deletions]