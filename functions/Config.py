
# THIS FUNCTION ALLOWS TO CONFIGURE THE PARAMETERS FROM THE COMMAND LINE
# AND PROPERLY PASS THEM TO THE CORRESPONDING FUNCTION CALLS
#
# INPUT : 
# 		  NONE
#
# VALUE : -CONFIG              : (DICT)  A DICTIONNARY THAT CONTAINS ALL NECESSARY THE PARAMETERS AND THEIR VALUES
#		

import os
import sys
import time

### import functions from the script in the same dir
from utils import *
from PrintHelp import *


def Config():
	
	config={}


	if len(sys.argv) == 1 or "--help" in sys.argv or "-h" in sys.argv:

		if "snv" in sys.argv:
			PrintHelp("snv")
		elif "cnv" in sys.argv:
			PrintHelp("cnv")
		else:
			PrintHelp("general")







	# configure the variant calling tool
	else:

		### just in case script path contains '-' or '--'
		sys.argv[0] = "None"
		
		

		if 'snv' in sys.argv:
			sys.argv.remove('snv')
			snv = True
			cnv = False

			if len(sys.argv) < 3:
				PrintHelp("snv")


		if 'cnv' in sys.argv:
			sys.argv.remove('cnv')
			snv = False
			cnv = True

			if len(sys.argv) < 3:
				PrintHelp("cnv")



		### default values for parameters
		config["min_base_quality"]    = 10
		config["min_read_quality"]    = 20
		config["min_mapping_quality"] = 20
		config["min_variant_umi"]     = 10 #5
		config["strand_bias_method"]  = "default"
		config["output"]              = os.getcwd()
		config['name']                = "sim"
		config["pileup"]              = "None"
		config["max_hp_length"]       = 7
		config["alpha"]               = 0.05

		config["max_noise_rate"]      = 0.05

		config["variants"]            = "None"
		config["depth"]               = 1000
		config["umi_length"]          = 12
		config["read_length"]         = 110

		config['platform']           = "ILLUMINA"



		### required parameters
		# input : path to the bam/sam file
		# fasta  : path to the fasta file
		# bed  : path to the bed file

		required = ['--input | -i', '--fasta | -f', '--bed | -b']
		for param in required:
			missing = False
			for arg in param.split(" | "):
				try:
					test = sys.argv.index(arg)
					missing = False
					break
				except:
					missing = True



			if missing:
				PrintTime('error', "\tThe parameter "+param+" is required but missing !\n\t\t\tExiting...")
				exit()
		


		PrintProgramName()
		PrintTime("console", "\tConfiguring Parameters...")

		# differentiate the '-' in fasta of the '-' for arguments
		try:
			fastaIdx = sys.argv.index("--fasta")

			fastaIdx+=1
			sys.argv[fastaIdx] = sys.argv[fastaIdx].replace("-", ":")
		except:
			try:
				fastaIdx = sys.argv.index("-f")

				fastaIdx+=1
				sys.argv[fastaIdx] = sys.argv[fastaIdx].replace("-", ":")
			except:
				pass




		# differentiate the '-' in bed of the '-' for arguments
		try:
			bedIdx = sys.argv.index("--bed")

			bedIdx+=1
			sys.argv[bedIdx] = sys.argv[bedIdx].replace("-", ":")
		except:
			try:
				bedIdx = sys.argv.index("-b")

				bedIdx+=1
				sys.argv[bedIdx] = sys.argv[bedIdx].replace("-", ":")
			except:
				pass
		



		# differentiate the '-' in input of the '-' for arguments
		try:
			inputIdx = sys.argv.index("--input")

			inputIdx+=1
			sys.argv[inputIdx] = sys.argv[inputIdx].replace("-", ":")
		except:
			try:
				inputIdx = sys.argv.index("-i")

				inputIdx+=1
				sys.argv[inputIdx] = sys.argv[inputIdx].replace("-", ":")
			except:
				pass





		# differentiate the '-' in output of the '-' for arguments
		try:
			outputIdx = sys.argv.index("--output")
		
			outputIdx+=1
			sys.argv[outputIdx] = sys.argv[outputIdx].replace("-", ":")
		except:
			try:
				outputIdx = sys.argv.index("-o")

				outputIdx+=1
				sys.argv[outputIdx] = sys.argv[outputIdx].replace("-", ":")
			except:
				pass



		# differentiate the '-' in pileup of the '-' for arguments
		try:
			pileupIdx = sys.argv.index("--pileup")

			pileupIdx+=1
			sys.argv[pileupIdx] = sys.argv[pileupIdx].replace("-", ":")
		except:
			try:
				pileupIdx = sys.argv.index("-p")

				pileupIdx+=1
				sys.argv[pileupIdx] = sys.argv[pileupIdx].replace("-", ":")
			except:
				pass






		### verify that no arguments are empty
		params_in_config = ['input', 'bed', 'fasta', 'min_base_quality', 'amp_factor', 'min_read_quality', 'min_mapping_quality', 'min_variant_umi', 'strand_bias_method', 'max_strand_bias', 'output', 'pileup', 'variants', 'alpha', 'max_hp_length', 'depth', 'umi_length', 'read_length', 'max_noise_rate', 'name', 'platform']
		params_mini = ['i', 'b', 'f', 'x', 'x', 'x', 'x', 'x', 'x', 'o', 'p', 'v', 'x', 'x', 'd', 'x', 'u', 'x', 'x', "n", "x"]
		params = ['--input | -i', '--bed | -b', '--fasta | -f', '--min_base_quality', '--amp_factor', '--min_read_quality', '--min_mapping_quality', '--min_variant_umi', '--strand_bias_method', '--max_strand_bias', '--output | -o', '--pileup | -p', '--variants | -v', '--alpha' , '--max_hp_length', '--depth | -d', '--umi_length | -u', '--read_length', '--max_noise_rate', '--name | -n', '--platform']

		pointer = 1
		while pointer < len(sys.argv):
			param = sys.argv[pointer]
			try:
				value = sys.argv[pointer+1]
			except:
				if "--" in param:
					if param in ['--input', '--bed', '--fasta']:
						PrintTime('error', "\tThe parameter "+params[params_in_config.index(param.replace("--", ""))]+" is required and cannot be empty !\n\t\t\tExiting...")
						exit()
					else:
						if param.replace("--", "") in params_in_config:
							PrintTime('error', "\tThe parameter's "+params[params_in_config.index(param.replace("--", ""))]+" value cannot be empty !\n\t\t\tExiting...")
							exit()
						else:
							PrintTime('error', "\tThe parameter "+param+" is unknown !\n\t\t\tExiting...")
							exit()
				elif "-" in param and "--" not in param:
					if param in ['-i', '-b', '-f']:
						PrintTime('error', "\tThe parameter "+params[params_mini.index(param.replace("-", ""))]+" is required and cannot be empty !\n\t\t\tExiting...")
						exit()
					else:
						if param.replace("-", "") in params_mini:
							PrintTime('error', "\tThe parameter's "+params[params_mini.index(param.replace("-", ""))]+" value cannot be empty !\n\t\t\tExiting...")
							exit()
						else:
							PrintTime('error', "\tThe parameter "+param+" is unknown !\n\t\t\tExiting...")
							exit()
				else:
					PrintTime('error', "\tThe parameter "+param+" is unknown !\n\t\t\tExiting...")
					exit()


			if "-" in value and len(value) == 2:
				PrintTime('error', "\tThe parameter "+param+" is required and cannot be empty !\n\t\t\tExiting...")
				exit()

			if "--" in value and len(value) > 3:
				PrintTime('error', "\tThe parameter "+param+" is required and cannot be empty !\n\t\t\tExiting...")
				exit()

			pointer += 2





		args=""
		for arg in sys.argv:
			args+=" "+arg


		args = args.replace("--", "|")
		args = args.replace("-", "|")

		args = args.split("|")
		del args[0]


		sb_set = False

		for arg in args:
			param = arg.split(" ")[0]
			value = arg.split(" ")[1]

			if param == "input" or param == "i":
				param = "input"
				config[param]=[]

				for val in value.split(','):
					if val[0] != "/":
						val = os.getcwd()+"/"+val.replace(":", "-")
					config[param].append(val.replace(":", "-"))

			if param == "fasta" or param == "f":
				param = "fasta"
				if value[0] != "/":
					value = os.getcwd()+"/"+value.replace(":", "-")
				config[param]=value.replace(":", "-")

			if param == "bed" or param == "b":
				param = "bed"
				if value[0] != "/":
					value = os.getcwd()+"/"+value.replace(":", "-")
				config[param]=value.replace(":", "-")


			if param == "pileup" or param == "p":
				param = "pileup"
				if value[0] != "/":
					value = os.getcwd()+"/"+value.replace(":", "-")
				config[param]=value.replace(":", "-")


			if param == "variants" or param == "v":
				param = "variants"
				if value[0] != "/":
					value = os.getcwd()+"/"+value.replace(":", "-")
				config[param]=value.replace(":", "-")


			if param == "output" or param == "o":
				param = "output"
				if value[0] != "/":
					value = os.getcwd()+"/"+value.replace(":", "-")
				config[param]=value.replace(":", "-")



			if param == "min_base_quality":
				try:
					config[param]=float(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a float or an integer !\n\t\t\tExiting...")
					exit()

			if param == "min_read_quality":
				try:
					config[param]=float(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a float or an integer !\n\t\t\tExiting...")
					exit()

			if param == "min_mapping_quality":
				try:
					config[param]=float(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a float or an integer !\n\t\t\tExiting...")
					exit()

			if param == "min_variant_umi":
				try:
					config[param]=float(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a float or an integer !\n\t\t\tExiting...")
					exit()

			if param == "strand_bias_method":
				value = str(value).replace("'", '').replace('"', "")
				try:
					test = value.index('default')
					config[param]=value
				except:
					try:
						test = value.index('torrent_suite')
						config[param]=value
					except:
						PrintTime('error', "\tThe parameter --"+param+" can only be set to \"default\" or \"torrent_suite\" !\n\t\t\tExiting...")
						exit()		

			if param == "max_strand_bias":
				try:
					config[param]=float(value)
					sb_set = True
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a float or an integer !\n\t\t\tExiting...")
					exit()

			if param == "alpha":
				try:
					config[param]=float(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a float or an integer !\n\t\t\tExiting...")
					exit()

			if param == "max_hp_length":
				try:
					config[param]=float(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a float or an integer !\n\t\t\tExiting...")
					exit()


			if param == "amp_factor":
				try:
					config[param]=float(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a float or an integer !\n\t\t\tExiting...")
					exit()



			if param == "depth" or param == "d":
				param = "depth"
				try:
					value = int(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value can only be 100, 1000, 10 000 or 100 000!\n\t\t\tExiting...")
					exit()


				if value >= 1000 and value <= 100000:
					config[param]=int(value)
				else:
					PrintTime('error', "\tThe parameter's --"+param+" value can only be 100, 500, 1000, 2000, 3000, 5000, 10000, 20000, 50000 or 100 000!\n\t\t\tExiting...")
					exit()




			if param == "umi_length" or param == "u":
				param = "umi_length"
				try:
					value = int(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be an 6 <= integer <= 15 !\n\t\t\tExiting...")
					exit()

				if value >= 6 and value <= 15:
					config[param]=int(value)
				else:
					PrintTime('error', "\tThe parameter's --"+param+" value should be an 6 <= integer <= 15 !\n\t\t\tExiting...")
					exit()


			if param == "read_length":
				try:
					value = int(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be an 80 <= integer <= 150 !\n\t\t\tExiting...")
					exit()

				if value >= 80 and value <= 150:
					config[param]=int(value)
				else:
					PrintTime('error', "\tThe parameter's --"+param+" value should be an 80 <= integer <= 150 !\n\t\t\tExiting...")
					exit()


			if param == "max_noise_rate":
				try:
					value = float(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a float <= 1 !\n\t\t\tExiting...")
					exit()

				if value >= 0 and value <= 1:
					config[param]=float(value)
				else:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a float <= 1 !\n\t\t\tExiting...")
					exit()


			if param == "name" or param == "n":
				param = "name"
				try:
					config[param]=str(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a string !\n\t\t\tExiting...")
					exit()


			if param == "platform":
				values = ['CAPILLARY', 'LS454', 'ILLUMINA', 'SOLID', 'HELICOS', 'IONTORRENT', 'ONT', 'PACBIO']
				try:
					test = values.index(value)
					config[param]=str(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be in the list below: \n\t\t\t['CAPILLARY', 'LS454', 'ILLUMINA', 'SOLID', 'HELICOS', 'IONTORRENT', 'ONT', 'PACBIO'] !\n\n\t\t\tExiting...")
					exit()

		if not sb_set:
			if config["strand_bias_method"] == "default":
				config["max_strand_bias"] = 1.0
			else:
				config["max_strand_bias"] = 0.743



		### calculate amp_factor based on depth
		max_unique_umis = 150.0
		min_amp_factor = int(round(config['depth']/2/max_unique_umis))
		if 'amp_factor' in config:
			if float(config['amp_factor']) < min_amp_factor:
				config['amp_factor'] = min_amp_factor
				PrintTime('warning', '\t\tAmplification factor is lower than minimum accepted value! Set to '+str(min_amp_factor))
		else:
			default_unique_umis = max_unique_umis/2
			default_amp_factor = int(round(config['depth']/2/default_unique_umis))
			config['amp_factor'] = default_amp_factor

		return config



