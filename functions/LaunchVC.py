#
# THIS FUNCTION ALLOWS TO LAUNCH UMI-VARCAL VARRIANT CALLER IN ORDER TO DETECT VARIANTS IN CONTROL SAMPLES
#
# INPUT : -PILEUP_MINI       : (DICT) THE UPDATED MINI DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC FOR THE CONTROL SAMPLES
#         -SAMPLE            : (DICT) NAME OF THE SAMPLE BEING ANALYZED
#         -SAMPLE_NUM        : (DICT) NUMBER OF THE SAMPLE BEING ANALYZED
#         -CONFIG            : (DICT) DICTIONNARY CONTAINING THE PARAMETERS OF THE VARIANT CALLING ANALYSIS
#
# VALUE : -PILEUP_MINI       : (DICT) THE UPDATED MINI DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC FOR THE CONTROL SAMPLES
#  	 	  -FINALVARIANTS     : (DICT) DICTIONNARY CONTAINING ALL THE VARIANTS FOUND IN THE CONTROL SAMPLES

# import python modules
import os
import sys
import time
import math
import msgpack
from pyfasta import Fasta
from collections import OrderedDict

# import local modules
from utils import *
from TreatReads_func import TreatReads
from FilterPositions import *
from CallVariants import *
from ParseBED import *

# from functions import *

def LaunchVC(pileup_mini, sample, sample_num, config, sim_type):

	# load and define variables from the config
	BED                  = config['bed']
	FASTA                = config['fasta']
	MIN_BASE_QUALITY     = int(config['min_base_quality'])
	MIN_MAPPING_QUALITY  = int(config['min_mapping_quality'])
	MIN_READ_QUALITY     = int(config['min_read_quality'])
	MIN_VARIANT_UMI      = int(config['min_variant_umi'])
	STRAND_BIAS_METHOD   = str(config['strand_bias_method'])
	MAX_STRAND_BIAS      = float(config['max_strand_bias'])
	PILEUP               = config['pileup']
	REBUILD              = False if os.path.isfile(PILEUP) else True
	OUTPUT               = config['output']
	ALPHA                = float(config['alpha'])
	MAX_HP_LENGTH        = int(config['max_hp_length'])
	MAX_NOISE_RATE       = float(config["max_noise_rate"])



	# load the reference genome file
	f = Fasta(FASTA)



	# if input is bam => launch samtools view command
	# to convert it to sam
	if ".bam" in sample and ".sam" not in sample:

		print("\n")
		PrintTime('console', "\tConverting BAM to SAM...")

		SAM = BAMtoSAM(sample)

		PrintTime('console', "\tDone")

	else:
		# else => sam = input
		SAM = sample




	# get total number of reads
	totalLines = GetTotalLines(SAM)



	pileup = ParseBED(BED)




	print("\n")
	PrintTime('console', "\tBuilding Pileup for sample #"+str(sample_num)+" ("+SAM.split('/')[-1].replace('.sam', '')+")...")
 	



	# get all UMI list
	ALL_UMIS = GetUMIS(SAM)




	# if only one core is to used, launch the function from here since no need to merge
	value = TreatReads(pileup, pileup_mini, SAM, BED, MIN_BASE_QUALITY, MIN_READ_QUALITY, MIN_MAPPING_QUALITY, ALL_UMIS)
	pileup = value[0]
	pileup_mini = value[1]



	# add depth to pileup
	pileup = AddDepths(pileup)
	pileup_mini = AddDepths(pileup_mini)

	# add variant error noise ate each position
	pileup = EstimateNoise(pileup)
	pileup_mini = CopyNoise(pileup, pileup_mini)

	# add reference bases in the dictionnary
	pileup = AddRefBases(pileup, f)
	pileup_mini = AddRefBases(pileup_mini, f)

	# add homopolymers infos
	pileup = AddHomoPolymers(pileup, f)
	pileup_mini = AddHomoPolymers(pileup_mini, f)



	print("\n")
	PrintTime('console', "\tDone")



	finalVariants = None
	if sim_type == "snv":

		### Poisson modeling to filter positions
		result = FilterPositions(pileup, ALPHA)
		pileup = result[0]
		potential = result[1]



		### call final variants
		finalVariants = CallVariants(pileup, f, STRAND_BIAS_METHOD, MAX_STRAND_BIAS, MIN_VARIANT_UMI, MAX_HP_LENGTH)


		### add positions with high VAF to variants list
		for chrom in pileup.keys():
			for position, composition in pileup[chrom].items():
				if pileup[chrom][position]['VAF'] >= MAX_NOISE_RATE:
					finalVariants[chrom+"|v1"] = {position: ["", ""]}
	

	return [pileup_mini, finalVariants]
















































