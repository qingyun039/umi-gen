import re
import os
from collections import OrderedDict
import random
import time
import math
import numpy
import sys
import pysam
import datetime
from collections import defaultdict
import operator
from scipy.stats import poisson
import statsmodels.stats.multitest as smm
import msgpack
from pyfasta import Fasta
from TreatReads_func import TreatReads


#
# THIS FUNCTION WILL APPLY MULTIPLE FILTERS TO THE VARINATS THAT PASS THE POISSON TEST IN ORDER TO REDUCE FALSE POSITIVES 
# 
# INPUT : 
#		 -PILEUP			  : (DICT)   THE DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
#		  -F				   : (DICT)   A DICTIONNARY CONTAINING THE REFERENCE BASES AT ALL POSITIONS OF THE GENOME 
# 		  -SB_METHOD		   : (STR)	DEFAULT METHOD for SB CALCULATION OR TORRENT SUITE METHOD
# 		  -MAX_STRAND_BIAS	 : (FLOAT)  THRESHOLD FOR A VARIANT TO BE CONSIDERED AS STRAND BIASED
# 		  -MIN_VARIANT_UMI	 : (INT)	THRESHOLD for A VARIANT WITH A CERTAIN UMI COUNT TO BE CALLED
# 		  -MAX_HP_LENGTH	   : (INT)	HOMOPOLYMER REGION LENGTH THRESHOLD FOR A VARIANT IN IT TO BE CALLED 
#
# VALUE : -FINALVARIANTS	   : (DICT)   A DICTIONNARY CONTAINING ONLY THE FINAL VARIANTS THAT SUCCESSFULLY PASSED ALL FILTERS
#



def CallVariants(pileup, f, SB_METHOD, MAX_STRAND_BIAS, MIN_VARIANT_UMI, MAX_HP_LENGTH):

	# create a dictionnary to contain only final variants
	finalVariants = {}

	print("\n")
	PrintTime('console', "\tCalling Variants...")

	# define counters to calculate progress
	currentPos = 1.0
	lastProgress = 0.0
	totalPos = float(GetPileupLength(pileup))


	# loop through the PILEUP dictionnary
	# for each chromosome
	for chrom , infos in pileup.items():
		# try-except block for first variants
		# if chromosome already in finalVariants - first variant - dict
		try:
			# do nothing
			test = finalVariants[chrom+"|v"]
		
		# if chromosome not in finalVariants - first variant - dict
		except:
			# add the chromosome to the finalVariants - first variant - dict
			finalVariants[chrom+"|v"] = {}


		# try-except block for second variants
		# if chromosome already in finalVariants - second variant - dict
		try:
			# do nothing
			test = finalVariants[chrom+"|v2"]
		
		# if chromosome not in finalVariants - second variant - dict
		except:
			# add the chromosome to the finalVariants - second variant - dict
			finalVariants[chrom+"|v2"] = {}


		# try-except block for third variants
		# if chromosome already in finalVariants - third variant - dict
		try:
			# do nothing
			test = finalVariants[chrom+"|v3"]
		
		# if chromosome not in finalVariants - third variant - dict
		except:
			# add the chromosome to the finalVariants - third variant - dict
			finalVariants[chrom+"|v3"] = {}



		# for each position | composition = counts + infos (ref_allele, alt_allele, q-value, ...)
		for position, composition in infos.items():

			# function that calculates and displays progress
			lastProgress = PrintProgress(currentPos, totalPos, lastProgress)

			# retrieve list of reference UMIs from PILEUP dictionnary
			ref_umi_len = len(composition[composition['ref']][2])
			ref_umi = list(set(composition[composition['ref']][2]))



			# make list to contain all the possible alternative alleles
			alt_list = [composition['alt']]
			# another list to contain the indexes ["", "2", "3"]
			alt_indexes = {composition['alt']: ""}

			if composition['alt2'] != None:
				alt_list.append(composition['alt2'])
				alt_indexes[composition['alt2']] = "2"
			if composition['alt3'] != None:
				alt_list.append(composition['alt3'])
				alt_indexes[composition['alt3']] = "3"




			# for each possible alternative allele 
			for alt_element in alt_list:

				# get corresponding index
				alt_index = alt_indexes[alt_element]

				# retrieve list of variant UMIs from PILEUP dictionnary
				# if variant is insertion or deletion
				if alt_element == 'in' or alt_element == 'del':

					
					umi_list = []

					# for each insertion or deletion
					for indel, infos in composition[alt_element].items():
						# append the UMI to the umi list
						umi_list += infos[2]

				# else if variant is substitution
				else:
					# retrieve list of variant UMIs from PILEUP dictionnary
					umi_list = composition[alt_element][2]

				

				# remove variant UMIs that occurs only once and keep unique UMIs only
				alt_umi = list(set([i for i in umi_list if umi_list.count(i) > 1]))


				# create the list of unique noise UMIs
				# noise UMIs are the UMIs found on reads that had neither the reference base nor the variant
				alphabet = ["A", "C", "G", "T", "in", "del"]
				alphabet.remove(composition['ref'])
				alphabet.remove(alt_element)
				noise_umi = set()
				for c in alphabet:
					if c == 'in' or c == 'del':
						for indel, infos in composition[c].items():
							noise_umi.update(infos[2])
					else:
						noise_umi.update(composition[c][2])



				#calculate total + coverage
				pos_covTotal = composition['A'][0]+composition['C'][0]+composition['G'][0]+composition['T'][0]
				
				#calculate total - coverage
				neg_covTotal = composition['A'][1]+composition['C'][1]+composition['G'][1]+composition['T'][1]

				# add insertion + and - coverage to total + and - coverage
				for indel, infos in composition['in'].items():
					pos_covTotal += infos[0]
					neg_covTotal += infos[1]

				# add deletion + and - coverage to total + and - coverage
				for indel, infos in composition['del'].items():
					pos_covTotal += infos[0]
					neg_covTotal += infos[1]

				# retrieve variant + and - coverage
				# if variant is insertion or deletion => go through all insertions / deletions 
				if alt_element == 'in' or alt_element == 'del':
					pos_covAllele = 0
					neg_covAllele = 0
					for indel, infos in composition[alt_element].items():
						pos_covAllele += infos[0]
						neg_covAllele += infos[1]

				# else if variant is substitution, retrieve from PILEUP dictionnary
				else:
					pos_covAllele = composition[alt_element][0]
					neg_covAllele = composition[alt_element][1]

				

				#Strand bias computation ( we add 1 to avoid 0 values)
				Vp=float(pos_covAllele+1)
				Vm=float(neg_covAllele+1)
				
				Cp=float(pos_covTotal+1)
				Cm=float(neg_covTotal+1)
				
				SB = CalculateStrandBias(Cp, Cm, Vp, Vm, SB_METHOD)


				# ref discordant UMIs are UMIs that are found in the reference UMIs list
				# AND in the (variant|noise) UMIs list
				# ref concordant UMIs are UMIs that are found in the reference UMIs list ONLY
				ref_discordant = 0
				for umi in ref_umi:
					if umi in alt_umi or umi in noise_umi:
						ref_discordant += 1

				ref_concordant = len(ref_umi) - ref_discordant

				# alt discordant UMIs are UMIs that are found in the variant UMIs list
				# AND in the (reference|noise) UMIs list
				# alt concordant UMIs are UMIs that are found in the variant UMIs list ONLY
				alt_discordant = 0
				for umi in alt_umi:
					if umi in ref_umi or umi in noise_umi:
						alt_discordant += 1

				alt_concordant = len(alt_umi) - alt_discordant





				# add alt_concordant and SB informations to pileup
				pileup[chrom][position]['alt_concordant'+alt_index] = alt_concordant
				pileup[chrom][position]['alt_discordant'+alt_index] = alt_discordant
				pileup[chrom][position]['SB'+alt_index] = SB

				# Allelic frequency = AF 
				AF = composition['VAF'+alt_index]
				# DePth = DP
				DP = composition['depth']
				# Allele Observations = AO
				AO = int(round(float(AF)*float(DP), 0))
				# HomoPolymer length = HP
				HP = composition['HP']
				# get error_base_probability
				base_error_probability = composition['base_error_probability']
				# compute confidence level of the variant
				conf = ComputeConfidence(composition['q-value'+alt_index], alt_discordant, alt_concordant, HP, Cp, Cm, Vp, Vm)
				CONF = conf[1]
				CONF_EXTRA = conf[1]+" ("+str(conf[0])+"/5)"



				# if the potential variant at this position passes 4 final filters => true variant
				# filter 1 : number of alt_concordant UMIs >= MIN_VARIANT_UMI value 
				# filter 2 : comptuted strand bias must be <= MAX_STRAND_BIAS value
				# filter 3 : homopolymer length must be <= MAX_HP_LENGTH value
				# filter 4 : total number of umis must be > to concordant UMIS / VAF


				if SB <= MAX_STRAND_BIAS and HP <= MAX_HP_LENGTH and alt_concordant >= MIN_VARIANT_UMI and len(list(list(alt_umi)+list(noise_umi)))+ref_umi_len > (float(composition['alt_concordant'+alt_index])/composition['VAF'+alt_index]):
								
					# if the variant is not an indel
					if alt_element != "in" and alt_element != "del":

						# build index
						index = chrom+":"+str(position)
						# get its position
						positionInVCF = str(position)

						# build the variant name
						variantName = index+composition['ref']+">"+alt_element
						# give it the type SNV
						TYPE = 'SNV'

					# else if the variant is an insertion
					elif alt_element == "in":

						# build index (position in index is the position -1)
						index = chrom+":"+str(position-1)
						# gets its position (position in VCF is the position -1)
						positionInVCF = str(position-1)

						# choosing the most frequent insertion
						chosen_ins = ['', 0, 0]
						for ins, infos in pileup[chrom][position]['in'].items():
							if (infos[0]+infos[1]) > (chosen_ins[1]+chosen_ins[2]):
								chosen_ins = [ins, infos[0], infos[1]]

						# build the variant name
						variantName = index+f[chrom][position-1-1]+">"+f[chrom][position-1-1]+chosen_ins[0]
						# give it the type INS
						TYPE = 'INS'

					# else if the variant is a deletion
					else:
						# build index (position in index is the position -1)
						index = chrom+":"+str(position-1)
						# gets its position (position in VCF is the position -1)
						positionInVCF = str(position-1)

						# choosing the most frequent deletion
						chosen_del = ['', 0, 0]
						for dele, infos in pileup[chrom][position]['del'].items():
							if (infos[0]+infos[1]) > (chosen_del[1]+chosen_del[2]):
								chosen_del = [dele, infos[0], infos[1]]


						# get the reference base from the reference dictionnary at the position-1
						lastBasePos = f[chrom][position-1-1].upper()
						# get the deleted seq by going from position -> position+length 
						# of deleted sequence in the reference dictionnary
						dele = int(chosen_del[0])
						del_seq = ""
						while dele > 0:
							del_seq += f[chrom][position+len(del_seq)-1].upper()
							dele -= 1

						# build the variant name
						variantName = index+lastBasePos+del_seq+">"+lastBasePos
						# giv it the type DEL
						TYPE = 'DEL'

					# build the REF and ALT columns of the VCF file for each 
					# variant type (SNV | INS | DEL)
					if TYPE == "SNV":
						REF = composition['ref']
						ALT = alt_element
					elif TYPE == "DEL":
						REF = lastBasePos+del_seq
						ALT = lastBasePos
					else:
						REF = f[chrom][position-1-1]
						ALT = f[chrom][position-1-1]+chosen_ins[0]


					# create the INFO line | delimiter is ';'
					INFO = "AF="+str(AF)+";AO="+str(AO)+";DP="+str(DP)+";HP="+str(HP)+";TYPE="+str(TYPE)+";CONF="+CONF.upper()
					


					# calculate total insertion observations on both strands
					n_ins_pos = 0
					n_ins_neg = 0
					for value in composition['in'].values():
						n_ins_pos += value[0]
						n_ins_neg += value[1]
					n_ins = n_ins_pos+n_ins_neg
					
					# calculate total deletion observations on both strands
					n_del_pos = 0
					n_del_neg = 0
					for value in composition['del'].values():
						n_del_pos += value[0]
						n_del_neg += value[1]
					n_del = n_del_pos+n_del_neg

					# build the VCF line for each variant | delimiter = "\t"
					lineVCF = "\t".join([chrom, positionInVCF, variantName, REF, ALT, str(composition['q-value'+alt_index]), "PASS", INFO])
					
					# build the VARIANTS file line for each variant | delimiter = "\t"
					lineExtra = "\t".join([
						chrom, 
						positionInVCF, 
						"-", 
						REF, 
						index.replace(':', '-'), 
						str(composition['A'][0]+composition['A'][1]), 
						str(composition['C'][0]+composition['C'][1]), 
						str(composition['G'][0]+composition['G'][1]), 
						str(composition['T'][0]+composition['T'][1]), 
						"0", 
						"0", 
						str(n_ins), 
						str(n_del), 
						str(DP), 
						str((composition['A'][0]+composition['A'][1])/float(DP)),
						str((composition['C'][0]+composition['C'][1])/float(DP)),
						str((composition['G'][0]+composition['G'][1])/float(DP)),
						str((composition['T'][0]+composition['T'][1])/float(DP)),
						"0",
						str(n_ins/float(DP)),
						str(n_del/float(DP)),
						str((composition[composition['ref']][0]+composition[composition['ref']][1])/float(DP)),
						str(AO/float(DP)),
						"-", "-", "-", "-", "-", "-",
						"-", "-", "-", "-", "-", "-",
						"TRUE",
						alt_element,
						variantName.replace(':', 'g.'),
						"FALSE",
						str(len(ref_umi)),
						str(len(alt_umi)),
						" ", 
						" ", 
						" ", 
						" ",
						str(alt_discordant),
						str(alt_concordant),
						" ",
						str(base_error_probability),
						str(composition['qScore']),
						str(composition['p-value'+alt_index]),
						str(composition['q-value'+alt_index]),
						TYPE,
						str(SB),
						str(HP),
						CONF_EXTRA,
						str(pos_covAllele),
						str(neg_covAllele),
						str(pos_covTotal),
						str(neg_covTotal),
						])
					
					# add both lines to the finalvariants dictionnary
					finalVariants[chrom+"|v"+alt_index][position] = [lineVCF, lineExtra] 

			# increment to track progress
			currentPos += 1



	print("\n")
	PrintTime('console', "\tDone")

	# return finalVariants dictionnary
	return finalVariants


#!/usr/bin/python







def CNV(config):
	
	# load and define variables from the config
	samples			  = config['input']
	BED				  = config['bed']
	FASTA				= config['fasta']
	VARIANTS			 = config["variants"]
	MIN_BASE_QUALITY	 = int(config['min_base_quality'])
	MIN_MAPPING_QUALITY  = int(config['min_mapping_quality'])
	MIN_READ_QUALITY	 = int(config['min_read_quality'])
	MIN_VARIANT_UMI	  = int(config['min_variant_umi'])
	STRAND_BIAS_METHOD   = str(config['strand_bias_method'])
	MAX_STRAND_BIAS	  = float(config['max_strand_bias'])
	PILEUP			   = config['pileup']
	REBUILD			  = False if os.path.isfile(PILEUP) else True
	OUTPUT			   = config['output']
	ALPHA				= float(config['alpha'])
	MAX_HP_LENGTH		= int(config['max_hp_length'])
	DEPTH				= int(config['depth'])
	AMP_FACTOR		   = int(config['amp_factor'])
	UMI_LENGTH		   = int(config['umi_length'])
	READ_LENGTH		  = int(config['read_length'])
	MAX_NOISE_RATE	   = float(config['max_noise_rate'])
	OUTPUT_NAME		  = config['name']+"_cnv_"+str(DEPTH)


	try:
		os.mkdir(OUTPUT+"/"+OUTPUT_NAME)
	except:
		pass

	OUTPUT = OUTPUT+"/"+OUTPUT_NAME


	# print parameters in the console
	if len(samples) == 1:
		PrintTime("green", "\t\tINPUT file   : "+samples[0])
	else:
		PrintTime("green", "\t\tSAMPLES #	: "+str(len(samples)))

	PrintTime("green", "\t\tBED file	 : "+BED)
	PrintTime("green", "\t\tFASTA file   : "+FASTA)

	if PILEUP != "None":	
		PrintTime("green", "\t\tPILEUP file  : "+PILEUP)
	PrintTime("green", "\t\tOutput	   : "+OUTPUT+"/"+OUTPUT_NAME)

	if VARIANTS != "None":
		PrintTime("green", "\t\tVAR file	 : "+VARIANTS)

	PrintTime("green", "\t\tmin_base_quality	  : "+str(MIN_BASE_QUALITY))
	PrintTime("green", "\t\tmin_read_quality	  : "+str(MIN_READ_QUALITY))
	PrintTime("green", "\t\tmin_mapping_quality   : "+str(MIN_MAPPING_QUALITY))
	PrintTime("green", "\t\tmin_variant_umi	   : "+str(MIN_VARIANT_UMI))
	PrintTime("green", "\t\tstrand_bias_method	: "+str(STRAND_BIAS_METHOD))
	PrintTime("green", "\t\tmax_strand_bias	   : "+str(MAX_STRAND_BIAS))
	PrintTime("green", "\t\tmax_hp_length		 : "+str(MAX_HP_LENGTH))
	PrintTime("green", "\t\talpha				 : "+str(ALPHA))

	PrintTime("green", "\t\tmax_noise_rate		: "+str(MAX_NOISE_RATE))

	PrintTime("green", "\t\tdepth				 : "+str(DEPTH))
	PrintTime("green", "\t\tamp_factor			: "+str(AMP_FACTOR))
	PrintTime("green", "\t\tumi_length			: "+str(UMI_LENGTH))
	PrintTime("green", "\t\tread_length		   : "+str(READ_LENGTH))

	PrintTime("console", "\tDone\n")



	variants = set([])
	pileup = ParseBED(BED)
	f = Fasta(FASTA)


	if PILEUP == "None":

		### create d for qscores at each position
		qScores = {}
		for chrom in pileup.keys():
			for pos in pileup[chrom].keys():
				try:
					qScores[chrom][pos] = []
				except:
					qScores[chrom] = {pos: []}


		for sample in samples:
			value = LaunchVC(pileup, sample, samples.index(sample)+1, config, "cnv")
			pileup = value[0]

			for chrom in pileup.keys():
				for pos in pileup[chrom].keys():
					qScores[chrom][pos].append(pileup[chrom][pos]['qScore'])

			# for chrom in value[1].keys():
			# 	for pos in value[1][chrom].keys():
			# 		chrom = chrom.split('|')[0]
			# 		variants.add(chrom+"-"+str(pos))


		AddNoiseToPileup(pileup, variants, qScores)


		try:
			os.mkdir('pileups')
		except:
			pass
		
		with open("pileups/cnv.pileup", 'wb') as handle:
			msgpack.pack(pileup, handle, encoding="utf-8")

	else:

		print("\n")
		PrintTime('console', "\tLoading Pileup...")

		with open(PILEUP, 'rb') as handle:
			pileup = msgpack.unpack(handle, encoding="utf-8")

		PrintTime('console', "\tDone")




	if VARIANTS == "None":
		variations = {}
		addVariants = False
	else:

		print("\n")
		PrintTime('console', "\tAnalyzing Variants...")

		addVariants = True
		variations = ParseVariationsFile(VARIANTS)


		PrintTime('console', "\tDone")



	value = GenerateReferenceReads_CNV(pileup, variations, BED, DEPTH, AMP_FACTOR, UMI_LENGTH, READ_LENGTH)
	pileup = value[0]
	reads = value[1]
	usedUMIS = value[2]

	
	# value = AddNoiseToReads(pileup, reads, BED, READ_LENGTH)
	# pileup = value[0]
	# reads = value[1]



	Output_CNV(reads, FASTA, OUTPUT, OUTPUT_NAME)











# THIS FUNCTION ALLOWS TO PARSE A CIGAR SEGMENT CONTAINING ONLY DELETED BASES AND INCREMENT
# A SPECIFIC PILEUP DICTIONNARY ACCORDINGLY
#
# INPUT : -PILEUP			: (DICT) THE DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
# 		  -UMI			   : (STR)  UMI SEQUENCE OF THE READ
# 		  -CHROM			 : (STR)  THE CHROMOSOME MAPPED TO THE READ
# 		  -START			 : (INT)  THE START POSITION OF THE READ
# 		  -SEQ			   : (STR)  THE SEQUENCE OF THE READ
# 		  -STRAND			: (INT)  THE STRAND OF THE READ (0 = FORWARD | 1 = REVERSE)
# 		  -CURSOR_POS		: (INT)  THE CURRENT POSITION IN THE READ
# 		  -CURSOR_SEQ		: (INT)  THE CURRENT POSITION IN THE SEQUENCE OF THE READ
# 		  -MAXX			  : (INT)  THE LENGTH OF THE CIGAR 'D' ELEMENT
#		  -ALL_UMIS		  : (DICT) A DICTIONNARY CONTAINING UMIS INDEXES
#
# VALUE : -POSITION		  : (INT)  THE FINAL POSITION THAT'S BEEN PARSED IN THE READ
#		  -CURSOR_POS		: (INT)  THE CURRENT POSITION IN THE READ
# 		  -CURSOR_SEQ		: (INT)  THE CURRENT POSITION IN THE SEQUENCE OF THE READ
#

def AddDeletions(pileup, pileup_mini, umi, chrom, start, seq, strand, cursor_pos, cursor_seq, maxx, ALL_UMIS):

	umi = ALL_UMIS[umi]

	# create a cursor to move in the deleted sequence 
	del_cursor = 0

	# for each position between limits
	for position in range(start+cursor_pos, start+cursor_pos+maxx):
		
		# try because base position in read could not be in BED file
		# therefore, the position could not be in the PILEUP dictionnary
		# if position not in BED, skip the read
		try:
			# test if chrom and position are in the PILEUP dictionnary
			test = pileup[chrom][position]
			
			# if this deletion was already seen at this location
			# maxx-del_cursor corresponds to the length of the deleted sequence
			# this number will serve as an entry in the PILEUP dictionnary for
			# deleted sequences
			try:
				# increment the corresponding counter in the PILEUP dictionnary
				pileup[chrom][position]['del'][maxx-del_cursor][strand] += 1
				pileup_mini[chrom][position]['del'][maxx-del_cursor][strand] += 1
				# add the umi to the corresponding list specific to the deleted sequence
				pileup[chrom][position]['del'][maxx-del_cursor][2].append(umi)

			# if this is the first time we see this deleted sequence at this position, 
			# an entry should be created with its appropriated structure (2 counters + set)  
			except:
				# create the entry in PILEUP dictionnary
				pileup[chrom][position]['del'][maxx-del_cursor] = [0, 0, [umi]]
				pileup_mini[chrom][position]['del'][maxx-del_cursor] = [0, 0, []]
				# increment the corresponding counter in the PILEUP dictionnary
				pileup[chrom][position]['del'][maxx-del_cursor][strand] += 1
				pileup_mini[chrom][position]['del'][maxx-del_cursor][strand] += 1


			
			# advance in the deleted sequence if the deletion length > 1
			del_cursor += 1


		# if position not in BED, skip the read
		except:
			pass	


	# increment the cursor_pos with the length of the cigar element when done adding it
	cursor_pos += maxx

	# return position, cursor_seq and cursor_pos to continue from where we left off
	return [position, cursor_seq, cursor_pos]











# THIS FUNCTION ALLOWS TO PARSE A CIGAR SEGMENT CONTAINING ONLY DELETED BASES AND INCREMENT
# A SPECIFIC PILEUP DICTIONNARY ACCORDINGLY
#
# INPUT : -PILEUP			: (DICT) THE DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
# 		  -UMI			   : (STR)  UMI SEQUENCE OF THE READ
# 		  -STRAND			: (INT)  THE STRAND OF THE READ (0 = FORWARD | 1 = REVERSE)
# 		  -CHROM			 : (STR)  THE CHROMOSOME MAPPED TO THE READ
# 		  -START			 : (INT)  THE START POSITION OF THE READ
#		  -CIGAR			 : (STR)  THE CIGAR SEQUENCE OF THE READ
# 		  -SEQ			   : (STR)  THE SEQUENCE OF THE READ
# 		  -QUAL			  : (STR)  THE QUALITY STRING OF THE READ
# 		  -QUALS			 : (DICT) A DICTIONNARY FOR THE CONVERSION OF THE QUALITIES FROM ASCII TO INT
# 		  -MIN_BASE_QUALITY  : (INT)  MINIMUM QUALITY SCORE OF THE BASE FOR IT TO ADDED TO THE PILEUP DICTIONNARY
#		  -ALL_UMIS		  : (DICT) A DICTIONNARY CONTAINING UMIS INDEXES
#
# VALUE : NONE
#		  


def AddRead(pileup, pileup_mini, umi, strand, chrom, start, cigar, seq, qual, quals, MIN_BASE_QUALITY, ALL_UMIS):

	# split the cigar sequence using any of the five characters as a delimiter:
	# M (match/mismatch), S (soft clip), I (insertion), 
	# D (deletion) or H (hard clip)
	# this will return a list with the lengths of the consecutive events
	cigar_lengths = re.split('M|S|I|D|H', cigar)
	
	# try removing the empty element at the end of the list created by splitting
	# if last character is empty
	try:
		# remove it
		cigar_lengths.remove('')

	# if last character is not empty
	# a character not in [M,I,S,D,H] is in the cigar element
	# don't know what to do => skip read 
	except:
		print('\n')
		PrintTime("error", "\t\tUnexpected character found in CIGAR ("+cigar+")... Read skipped !\n")
		return False


	# split the cigar sequence by any number
	# this will return a list with the consecutive events
	cigar_chars = re.split('[0-9]+', cigar)
	
	# remove the empty element at the end of the list created by splitting
	cigar_chars.remove('')
	

	# initialize a counter to increment position only without advancing in the sequence
	cursor_pos = 0
	
	# initialize a counter to advance in the sequence without incrementing the position
	cursor_seq = 0	 

	# the need of two seperate is for cases when the event is an insertion as we need to
	# advance in the sequence of the read without incrementing the position or when the 
	# event is a deletion as we need to increment the position without advancing in the
	# sequence of the read




	# for each cigar event in the cigar events list (M|S|I|D|H)
	for i in range(0, len(cigar_chars)):
		
		# if the event is a match/mismatch
		if cigar_chars[i] == "M":
			
			# get the length of the match/mismatch event
			maxx = int(cigar_lengths[i])

			# call the specific function responsible of parsing a match/mismatch segment of the read 
			value = AddMatches(pileup, pileup_mini, umi, chrom, start, seq, strand, cursor_pos, cursor_seq, maxx, qual, quals, MIN_BASE_QUALITY, ALL_UMIS)
			
			# get the returned values to continue from the position where the event ends
			position = value[0]
			cursor_seq = value[1]
			cursor_pos = value[2]
		
		# if the event is an insertion
		elif cigar_chars[i] == "I":
			
			# get the length of the insertion event
			maxx = int(cigar_lengths[i])

			# try to get position
			# normally, an read does not start with an insertion so the position of the insertion
			# is the last position of the previous cigar element 
			try:
				test = position

			# in some cases, the read starts with an insertion, therefore that position is not defined
			# in this case, an exception will be throw in order to attribute the start value to position
			except:
				position = start

			# call the specific function responsible of parsing an inserted segment of the read 
			value = AddInsertions(pileup, pileup_mini, umi, chrom, position, seq, strand, cursor_pos, cursor_seq, maxx, qual, quals, MIN_BASE_QUALITY, ALL_UMIS)
			
			# get the returned values to continue from the position where the event ends
			position = value[0]
			cursor_seq = value[1]
			cursor_pos = value[2]

		# if the event is a deletion
		elif cigar_chars[i] == "D":

			# get the length of the deletion event
			maxx = int(cigar_lengths[i])

			# call the specific function responsible of parsing a deleted segment of the read 
			value = AddDeletions(pileup, pileup_mini, umi, chrom, start, seq, strand, cursor_pos, cursor_seq, maxx, ALL_UMIS)

			# get the returned values to continue from the position where the event ends
			position = value[0]
			cursor_seq = value[1]
			cursor_pos = value[2]
		
		# if the event is a soft clip
		# soft clipping removes the clipped sequence from the read without correcting read start position
		elif cigar_chars[i] == "S":
			
			# test if the soft clip occurs at the beginning of the read
			try:
				# if not beginning of the read => cursor_pos > 0
				test = 20 / cursor_pos
				# in this case, do nothing since the soft clipping occured at the end of the read
				# since a clipping event can only occurs at the start or at the end of the read 
				continue

			# if beginning of the read => cursor_pos = 0 => exception 
			except:
				# get length of the clipped sequence
				clipped = int(cigar_lengths[i])
				# correct the read start position
				start -= clipped

				# increment the 2 cursors and continue
				cursor_pos += clipped
				cursor_seq += clipped

		# if the event is a hard clip
		# just continue (hard clipping corrects read start position and removes the clipped sequence from the read)
		elif cigar_chars[i] == "H":
			continue



















#
# THIS FUNCTION ALLOWS TO ADD TRUE VARIANTS TO THE READS
#
# INPUT : -READS			 : (DICT) A DICTIONNARY CONTAINING ALL OF R1 AND R2 REFERENCE READS
# 		  -FASTA			 : (STR) LOCATION OF THE REFERENCE GENOME FASTA FILE
#		 -OUTPUT			: (STR) DIRECTORY IN WHICH THE PRODUCED SAMPLES ARE CREATED
# 		  -OUTPUT_NAME	   : (STR) THE NAME OF THE PRODUCED SAMPLE
#  	 	  -INSERTIONS		: (DICT) DICTIONNARY CONTAINING ALL THE READS HAVING A SPECIFIC INSERTION
#		 -DELETIONS		 : (DICT) DICTIONNARY CONTAINING ALL THE READS HAVING A SPECIFIC DELETION
#
# VALUE : 
#		   NONE



def Output(reads, FASTA, output, output_name, insertions, deletions, NAME, PLATFORM):

	# print output
	# print output_name
	
	AlphaToLen = {
		"del": {"b": 1, "d": 2, "e": 3, "f": 4, "h": 5, "i": 6, "j": 7, "k": 8, "l": 9, "m": 10}, 
		"in" : {"n": 1, "o": 2, "p": 3, "q": 4, "r": 5, "s": 6, "u": 7, "v": 8, "w": 9, "x": 10}
	}


	
	totalPos = float(GetTotalPos2(reads["R1"])*2)
	currentPos = 1.0
	lastProgress = 0.0

	print("\n")
	PrintTime("console", "\tGenerating FASTQ...")

	# => fastq_R1
	output_R1 = open(output+"/"+output_name+"_R1.fastq", 'w')

	for x in reads["R1"].keys():
		y = x.split("-")
		chrom = y[0]
		start = int(y[1])
		end   = int(y[2])

		searchForInDel = False
		for del_index in deletions.keys():
			if int(del_index.split(":")[1]) in range(start, end):
				searchForInDel = True
				break

		for in_index in insertions.keys():
			if int(in_index.split(":")[1]) in range(start, end):
				searchForInDel = True
				break

		for read_id, read in reads["R1"][x].items():

			lastProgress = PrintProgress(currentPos, totalPos, lastProgress)

			readArray = read.split('|')
			read = readArray[0]
			eventLen = False
			event = False
			
			if searchForInDel:
				for base in read:
					if base not in 'ACGTacgt':
						base_idx = read.index(base)
						try:
							eventLen = AlphaToLen["del"][base]
							event = "del"
							read = read[0:base_idx]+deletions[del_index][1]+read[base_idx+1+eventLen:]

						except:
							eventLen = AlphaToLen["in"][base]
							event = "in"

							read = read[0:base_idx]+insertions[in_index][0]+insertions[in_index][1]+read[base_idx+1:]


			read_qual = readArray[1]
			if eventLen:
				if event == "in":
					read_qual = read_qual[0:base_idx]+read_qual[base_idx]*(eventLen+1)+read_qual[base_idx+1:]
				else:
					read_qual = read_qual[0:base_idx]+read_qual[base_idx]+read_qual[base_idx+1+eventLen:]


			read = read.upper()
			line = read_id+"\n"+read+"\n"+"+\n"+read_qual+"\n"
			output_R1.write(line)

			currentPos += 1.0

	output_R1.close()





	# => fastq_R2
	output_R2 = open(output+"/"+output_name+"_R2.fastq", 'w')

	for x in reads["R2"].keys():
		y = x.split("-")
		chrom = y[0]
		start = int(y[1])
		end   = int(y[2])

		searchForInDel = False
		for del_index in deletions.keys():
			if int(del_index.split(":")[1]) in range(start, end):
				searchForInDel = True
				break

		for in_index in insertions.keys():
			if int(in_index.split(":")[1]) in range(start, end):
				searchForInDel = True
				break

		for read_id, read in reads["R2"][x].items():

			lastProgress = PrintProgress(currentPos, totalPos, lastProgress)

			readArray = read.split('|')
			read = readArray[0]
			eventLen = False
			event = False
			
			if searchForInDel:
				for base in read:
					if base not in 'ACGTacgt':
						base_idx = read.index(base)
						try:
							eventLen = AlphaToLen["del"][base]
							event = "del"

							read = read[0:base_idx]+deletions[del_index][1]+read[base_idx+1+eventLen:]

						except:
							eventLen = AlphaToLen["in"][base]
							event = "in"

							read = read[0:base_idx]+insertions[in_index][0]+insertions[in_index][1]+read[base_idx+1:]


			read_qual = readArray[1]
			if eventLen:
				if event == "in":
					read_qual = read_qual[0:base_idx]+read_qual[base_idx]*(eventLen+1)+read_qual[base_idx+1:]
				else:
					read_qual = read_qual[0:base_idx]+read_qual[base_idx]+read_qual[base_idx+1+eventLen:]


			read = ReverseComplement(read)
			read = read.upper()
			read_qual = read_qual[::-1]
			line = read_id+"\n"+read+"\n"+"+\n"+read_qual+"\n"
			output_R2.write(line)

			currentPos += 1.0

	output_R2.close()






	print("\n")
	PrintTime("console", "\tDone")
	print("\n")


	# bwa => sam
	PrintTime("console", "\tConverting FASTQ to SAM")
	os.system('bwa mem -t 10 -R "@RG\tID:'+NAME+'\tPL:'+PLATFORM+'\tSM:'+NAME+'" '+FASTA+' '+output+"/"+output_name+'_R1.fastq '+output+"/"+output_name+'_R2.fastq > '+output+"/"+output_name+'.sam')
	PrintTime('console', '\tDone')
	print("\n")

	# samtools => bam
	PrintTime("console", "\tConverting SAM to BAM...")
	os.system('samtools view -Sb '+output+"/"+output_name+'.sam > '+output+"/"+output_name+'.bam')
	PrintTime('console', '\tDone')
	print("\n")

	# samtools => sorted_bam
	PrintTime("console", "\tSorting BAM...")
	os.system('samtools sort '+output+"/"+output_name+'.bam > '+output+"/"+output_name+'_sorted.bam')
	os.remove(output+"/"+output_name+".bam")
	os.rename(output+"/"+output_name+"_sorted.bam", output+"/"+output_name+".bam")
	PrintTime('console', '\tDone')
	print("\n")

	# samtools => index
	PrintTime("console", "\tIndexing BAM...")
	os.system('samtools index '+output+"/"+output_name+'.bam')
	PrintTime('console', '\tDone')



def Output_CNV(reads, FASTA, output, output_name, NAME, PLATFORM):

	
	totalPos = float(GetTotalPos2(reads["R1"])*2)
	currentPos = 1.0
	lastProgress = 0.0

	print("\n")
	PrintTime("console", "\tGenerating FASTQ...")

	# => fastq_R1
	output_R1 = open(output+"/"+output_name+"_R1.fastq", 'w')

	for x in reads["R1"].keys():
		y = x.split("-")
		chrom = y[0]
		start = int(y[1])
		end   = int(y[2])


		for read_id, read in reads["R1"][x].items():

			lastProgress = PrintProgress(currentPos, totalPos, lastProgress)

			readArray = read.split('|')
			read = readArray[0].upper()
			read_qual = readArray[1]



			line = read_id+"\n"+read+"\n"+"+\n"+read_qual+"\n"
			output_R1.write(line)

			currentPos += 1.0

	output_R1.close()





	# => fastq_R2
	output_R2 = open(output+"/"+output_name+"_R2.fastq", 'w')

	for x in reads["R2"].keys():
		y = x.split("-")
		chrom = y[0]
		start = int(y[1])
		end   = int(y[2])

		for read_id, read in reads["R2"][x].items():

			lastProgress = PrintProgress(currentPos, totalPos, lastProgress)

			readArray = read.split('|')
			read = readArray[0].upper()

		
			read_qual = readArray[1]
			

			read = ReverseComplement(read)
			read_qual = read_qual[::-1]
			line = read_id+"\n"+read+"\n"+"+\n"+read_qual+"\n"
			output_R2.write(line)

			currentPos += 1.0

	output_R2.close()






	print("\n")
	PrintTime("console", "\tDone")
	print("\n")


	# bwa => sam
	PrintTime("console", "\tConverting FASTQ to SAM")
	os.system('bwa mem -t 10 -R "@RG\tID:'+NAME+'\tPL:'+PLATFORM+'\tSM:'+NAME+'" '+FASTA+' '+output+"/"+output_name+'_R1.fastq '+output+"/"+output_name+'_R2.fastq > '+output+"/"+output_name+'.sam')
	PrintTime('console', '\tDone')
	print("\n")

	# samtools => bam
	PrintTime("console", "\tConverting SAM to BAM...")
	os.system('samtools view -Sb '+output+"/"+output_name+'.sam > '+output+"/"+output_name+'.bam')
	PrintTime('console', '\tDone')
	print("\n")

	# samtools => sorted_bam
	PrintTime("console", "\tSorting BAM...")
	os.system('samtools sort '+output+"/"+output_name+'.bam > '+output+"/"+output_name+'_sorted.bam')
	os.remove(output+"/"+output_name+".bam")
	os.rename(output+"/"+output_name+"_sorted.bam", output+"/"+output_name+".bam")
	PrintTime('console', '\tDone')
	print("\n")

	# samtools => index
	PrintTime("console", "\tIndexing BAM...")
	os.system('samtools index '+output+"/"+output_name+'.bam')
	PrintTime('console', '\tDone')



def GenerateReferenceReads_CNV(pileup, variations, BED, INI_DEPTH, AMP_FACTOR, UMI_LENGTH, READ_LENGTH):

	# calculte average depth of the sample so we can calculate depth ratio after 
	sampleDepth = 0
	totalPos = 0
	for chrom in pileup.keys():
		for pos in pileup[chrom].keys():
			sampleDepth += pileup[chrom][pos]['depth']
			totalPos += 1

	sampleDepth = int(round(float(sampleDepth)/float(totalPos), 0))


	
	ranges_d = ParseRanges(BED, READ_LENGTH)


	currentPos = 1.0
	lastProgress = 0.0
	totalPos = float(GetTotalPos1(ranges_d))

	quals_str = "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJ"
	quals = {}


	i=0
	for q in quals_str:
		quals[i] = quals_str[i]
		i += 1



	reads = {"R1": OrderedDict(), "R2": OrderedDict()}


	read_counter = 0 
	usedUMIS = set([])

	print("\n")
	PrintTime('console', '\tGenerating Reads...')



	for chrom, ranges in ranges_d.items():
		

		for aRange in ranges:

			regionInVariation = False 

			start = aRange[0]
			end   = aRange[1]

			if chrom in variations.keys():
				for var_region in variations[chrom]:
					var_start = int(var_region.split('-')[0])
					var_end = int(var_region.split('-')[1])

					# print aRange
					# print str(var_start) +" <= "+ str(start)
					# print str(var_end) +" >= "+ str(start)
					# print (var_start <= start and var_end >= start)
					# print "\n"
					# print str(var_start) +" >= "+ str(start)
					# print str(var_start) +" <= "+ str(end)
					# print (var_start >= start and var_start <= end)
					# print "\n"

					if len(set(range(start, end+1)).intersection(range(var_start, var_end+1))) > 0:
						regionInVariation = True
						var_chrom = chrom
						break




			DEPTH = INI_DEPTH


			if regionInVariation:

				# print(aRange)

				var_type = variations[var_chrom][var_region]['type']
				var_freq = variations[var_chrom][var_region]['freq']
				if var_type == "amp":
					DEPTH = INI_DEPTH * (1+var_freq)
				else:
					DEPTH = INI_DEPTH * (1-var_freq) 

				NEW_DEPTH = False
				# print(DEPTH)

			else:

				# calculate the range average depth in control samples
				totalDepth = 0
				for pos in range(start, end+1):
					totalDepth += pileup[chrom][pos]['depth']

				rangeDepthRatio = totalDepth/(end-start+1)/sampleDepth
				# print(rangeDepthRatio)
				NEW_DEPTH = rangeDepthRatio * INI_DEPTH





			try:
				test = reads['R1'][chrom+"-"+str(start)+"-"+str(end)]
			except:
				reads['R1'][chrom+"-"+str(start)+"-"+str(end)] = OrderedDict()
				reads['R2'][chrom+"-"+str(start)+"-"+str(end)] = OrderedDict()


			read = ""
			read_qual = ""







			for pos in range(start, end+1):




				if NEW_DEPTH:
					max_umis = float(INI_DEPTH)/2/float(AMP_FACTOR)
					DEPTH = NEW_DEPTH
					if DEPTH > INI_DEPTH:
						min_max_umis = math.floor(max_umis)
						max_max_umis = math.ceil(1.05 * max_umis)
						max_umis = random.choice(range(min_max_umis, max_max_umis))
					elif DEPTH < INI_DEPTH:
						min_max_umis = math.floor(0.95 * max_umis)
						max_max_umis = math.ceil(max_umis)
						max_umis = random.choice(range(min_max_umis, max_max_umis))
					# print(DEPTH)
					# print(max_umis)
					# print("\n")
					# time.sleep(0.1)
				else:
					max_umis = float(DEPTH)/2/float(AMP_FACTOR)
					# print("variation")
					# print(DEPTH)
					# print(max_umis)
					# print('\n')
					# time.sleep(1)



				lastProgress = PrintProgress(currentPos, totalPos, lastProgress)

				pileup[chrom][pos]['A'][0] = round(pileup[chrom][pos]['A'][0]*DEPTH, 0)
				pileup[chrom][pos]['A'][1] = round(pileup[chrom][pos]['A'][1]*DEPTH, 0)

				pileup[chrom][pos]['C'][0] = round(pileup[chrom][pos]['C'][0]*DEPTH, 0)
				pileup[chrom][pos]['C'][1] = round(pileup[chrom][pos]['C'][1]*DEPTH, 0)

				pileup[chrom][pos]['G'][0] = round(pileup[chrom][pos]['G'][0]*DEPTH, 0)
				pileup[chrom][pos]['G'][1] = round(pileup[chrom][pos]['G'][1]*DEPTH, 0)

				pileup[chrom][pos]['T'][0] = round(pileup[chrom][pos]['T'][0]*DEPTH, 0)
				pileup[chrom][pos]['T'][1] = round(pileup[chrom][pos]['T'][1]*DEPTH, 0)



				n_umis   = 0





				while n_umis < max_umis:
					umi = GenerateUMI(UMI_LENGTH)
					
					if umi not in usedUMIS:
						pileup[chrom][pos][pileup[chrom][pos]['ref']][2].append(umi)
						usedUMIS.add(umi)
						n_umis += 1




				read += pileup[chrom][pos]['ref']
				read_qual += quals[int(pileup[chrom][pos]['qScore'])]

				currentPos += 1.0
				



			# umi_pos = 0
			# umi_count = 0
			# for i in range(start, end):
			# 	x = len(pileup[chrom][i][pileup[chrom][i]['ref']][2])
			# 	if x > umi_count:
			# 		umi_pos = i
			# 		umi_count = x
			# 		pos_amp_factor = float(DEPTH)/x

			i = random.choice(range(start, end))
			umi_pos = i
			x = len(pileup[chrom][i][pileup[chrom][i]['ref']][2])
			pos_amp_factor = float(DEPTH)/x


			for umi in pileup[chrom][umi_pos][pileup[chrom][umi_pos]['ref']][2]:
				i = pos_amp_factor

				while i > 0:

					read_id = "@XXXXX:"+"0"*(15-len(str(read_counter)))+str(read_counter)+"_"+umi+" 1:N:0:1"
					reads["R1"][chrom+"-"+str(start)+"-"+str(end)][read_id] = read+"|"+read_qual
					i -= 1

					if i > 0:
						read_id = "@XXXXX:"+"0"*(15-len(str(read_counter)))+str(read_counter)+"_"+umi+" 2:N:0:1"
						reads["R2"][chrom+"-"+str(start)+"-"+str(end)][read_id] = read+"|"+read_qual

						i -= 1
		
					read_counter += 1



	toRemove = []
	for aRange in reads['R1'].keys():
		keys1 = reads['R1'][aRange]
		keys2 = reads['R2'][aRange]

		if len(keys1) > len(keys2):
			R1 = 'R1'; R2 = 'R2'; i1 = '1'; i2 = '2';
		else:
			R1 = 'R2'; R2 = 'R1'; i1 = '2'; i2 = '1';

		for key in reads[R1][aRange].keys():
			new_key = key.replace(' '+i1+':N:0:1', ' '+i2+':N:0:1')
			
			try:
				test = reads[R2][aRange][new_key]
			except:
				index=R1+"|"+aRange+"|"+key
				toRemove.append(index)
				

	for index in toRemove:
		index = index.split("|")
		R1 = index[0]
		aRange = index[1]
		key = index[2]
		del reads[R1][aRange][key]


	print("\n")
	PrintTime('console', '\tDone')


	return [pileup, reads, usedUMIS]



def AddVariationsToReads(pileup, variations, BED, DEPTH, AMP_FACTOR, UMI_LENGTH, READ_LENGTH):
	
	ranges_d = ParseRanges(BED, READ_LENGTH)


	currentPos = 1.0
	lastProgress = 0.0
	totalPos = float(GetTotalPos1(ranges_d))

	quals_str = "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJ"
	quals = {}


	i=0
	for q in quals_str:
		quals[i] = quals_str[i]
		i += 1



	reads = {"R1": OrderedDict(), "R2": OrderedDict()}


	read_counter = 0 
	usedUMIS = set([])

	print("\n")
	PrintTime('console', '\tGenerating Reference Reads...')

	for chrom, ranges in ranges_d.items():
		
		for aRange in ranges:
			start = aRange[0]
			end   = aRange[1]

			try:
				test = reads['R1'][chrom+"-"+str(start)+"-"+str(end)]
			except:
				reads['R1'][chrom+"-"+str(start)+"-"+str(end)] = OrderedDict()
				reads['R2'][chrom+"-"+str(start)+"-"+str(end)] = OrderedDict()


			read = ""
			read_qual = ""

			for pos in range(start, end+1):

				lastProgress = PrintProgress(currentPos, totalPos, lastProgress)

				pileup[chrom][pos]['A'][0] = round(pileup[chrom][pos]['A'][0]*DEPTH, 0)
				pileup[chrom][pos]['A'][1] = round(pileup[chrom][pos]['A'][1]*DEPTH, 0)

				pileup[chrom][pos]['C'][0] = round(pileup[chrom][pos]['C'][0]*DEPTH, 0)
				pileup[chrom][pos]['C'][1] = round(pileup[chrom][pos]['C'][1]*DEPTH, 0)

				pileup[chrom][pos]['G'][0] = round(pileup[chrom][pos]['G'][0]*DEPTH, 0)
				pileup[chrom][pos]['G'][1] = round(pileup[chrom][pos]['G'][1]*DEPTH, 0)

				pileup[chrom][pos]['T'][0] = round(pileup[chrom][pos]['T'][0]*DEPTH, 0)
				pileup[chrom][pos]['T'][1] = round(pileup[chrom][pos]['T'][1]*DEPTH, 0)



				n_umis   = 0
				
				### adapt amp factor and unique umis count depending on 
				### the variant frequency if position if mutated
				if chrom+":"+str(pos) in variations.keys():
					var_freq = variations[chrom+":"+str(pos)][1]
					var_reads = var_freq * DEPTH
					VAR_AMP_FACTOR = var_reads / 10


					if VAR_AMP_FACTOR > AMP_FACTOR:
						POS_AMP_FACTOR = AMP_FACTOR 
					else:
						POS_AMP_FACTOR = VAR_AMP_FACTOR

				else:
					POS_AMP_FACTOR = AMP_FACTOR


				# add variability so not all positions have exact AMP FACTOR
				# mean = POS_AMP_FACTOR
				# min = 0.85 * POS_AMP_FACTOR
				# max = 1.15 * POS_AMP_FACTOR
				MIN_POS_AMP_FACTOR = 0.85 * POS_AMP_FACTOR
				MAX_POS_AMP_FACTOR = 1.15 * POS_AMP_FACTOR
				POS_AMP_FACTOR = random.choice(numpy.arange(MIN_POS_AMP_FACTOR, MAX_POS_AMP_FACTOR, 0.05))


				max_umis = float(DEPTH)/2/float(POS_AMP_FACTOR)


				while n_umis < max_umis:
					umi = GenerateUMI(UMI_LENGTH)
					
					if umi not in usedUMIS:
						pileup[chrom][pos][pileup[chrom][pos]['ref']][2].append(umi)
						usedUMIS.add(umi)
						n_umis += 1




				read += pileup[chrom][pos]['ref']
				read_qual += quals[int(pileup[chrom][pos]['qScore'])]

				currentPos += 1.0
				



			# umi_pos = 0
			# umi_count = 0
			# for i in range(start, end):
			# 	x = len(pileup[chrom][i][pileup[chrom][i]['ref']][2])
			# 	if x > umi_count:
			# 		umi_pos = i
			# 		umi_count = x
			# 		pos_amp_factor = float(DEPTH)/x

			i = random.choice(range(start, end))
			umi_pos = i
			x = len(pileup[chrom][i][pileup[chrom][i]['ref']][2])
			pos_amp_factor = float(DEPTH)/x


			for umi in pileup[chrom][umi_pos][pileup[chrom][umi_pos]['ref']][2]:
				i = pos_amp_factor

				while i > 0:

					read_id = "@XXXXX:"+"0"*(15-len(str(read_counter)))+str(read_counter)+"_"+umi+" 1:N:0:1"
					reads["R1"][chrom+"-"+str(start)+"-"+str(end)][read_id] = read+"|"+read_qual
					i -= 1

					if i > 0:
						read_id = "@XXXXX:"+"0"*(15-len(str(read_counter)))+str(read_counter)+"_"+umi+" 2:N:0:1"
						reads["R2"][chrom+"-"+str(start)+"-"+str(end)][read_id] = read+"|"+read_qual

						i -= 1
		
					read_counter += 1




	print("\n")
	PrintTime('console', '\tDone')


	for aRange in reads['R1'].keys():
		keys1 = reads['R1'][aRange]
		keys2 = reads['R2'][aRange]

		if len(keys1) > len(keys2):
			R1 = 'R1'; R2 = 'R2'; i1 = '1'; i2 = '2';
		else:
			R1 = 'R2'; R2 = 'R1'; i1 = '2'; i2 = '1';

		for key in reads[R1][aRange].keys():
			if key.replace(' '+i1+':N:0:1', ' '+i2+':N:0:1') not in reads[R2][aRange].keys():
				del reads[R1][aRange][key]



	return [pileup, reads, usedUMIS]


# THIS FUNCTION ALLOWS TO PARSE A CIGAR SEGMENT CONTAINING ONLY DELETED BASES AND INCREMENT
# A SPECIFIC PILEUP DICTIONNARY ACCORDINGLY
#
# INPUT : 
# 		  -BED			   : (STR)  THE PATH OF THE BED FILE
#
# VALUE : -PILEUP			: (DICT) A DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
#		



def ParseBED(bed):	

	# create an empty dictionnary for pileup counters
	pileup = {}

	# read the bed file line by line
	for line in open(bed):

		# split the line using tab as delimiter
		line = line.split('\t')

		# first element is the chromosome
		chrom = line[0]

		# second element is the start of the region
		# third element is the end of the region
		# create a list containing start and end 
		# sort to ensure that start < end
		limits = [int(line[1]), int(line[2])]
		limits.sort()


		# for each position in the [start;end] interval
		for pos in range(limits[0], limits[1]+1):
			
			# try to add the position to the pileup for the specified chromosome
			# if the chromosome was already aded to the pileup
			try:
				# add the position to the chrom with the default counters
				pileup[chrom][pos] = { 'A': [0, 0,[]], 'C': [0, 0, []], 'G': [0, 0, []], 'T': [0, 0, []], 'in': {}, 'del': {}, 'base_error_probability': 0 } 
			except:
				# add the chrom to the pileup with the first position and the default counters
				pileup[chrom] = { pos: { 'A': [0, 0, []], 'C': [0, 0, []], 'G': [0, 0, []], 'T': [0, 0, []], 'in': {}, 'del': {}, 'base_error_probability': 0 } }


	# the pileup dictionnary contains :
	# for each chromosome:
	# 		for each position:
	#		{
	#			'A'   : 0 | 0 | list()
	#			'C'   : 0 | 0 | list()
	#			'T'   : 0 | 0 | list()
	#			'G'   : 0 | 0 | list()
	#			'in'  :	 {}
	#			'del' :	 {}
	#			'base_error_probability' : 0
	#		}
	# for the four normal bases, this structure 0 | 0 | set() is a list containing 
	# a counter for the forward strand, a counter for the reverse strand, and a set
	# that will contain the UMIs of the reads. 
	# a set was used instead of a list to contain unique UMIs only  
	#
	# the insertion dictionnary will be updated each time an insertion is encountered
	# and will end up looking like this:
	# {
	# 	'inserted_seq_1' : N1_+ | N1_- | list()
	# 	'inserted_seq_2' : N2_+ | N2_- | list()
	# 	'inserted_seq_n' : Nn_+ | Nn_- | list()
	# }
	#
	# the deletion dictionnary will be updated each time a deletion is encountered
	# and will end up looking like this:
	# {
	# 	len of the deleted segment_1 : N1_+ | N1_- | list()
	# 	len of the deleted segment_2 : N2_+ | N2_- | list()
	# 	len of the deleted segment_n : Nn_+ | Nn_- | list()
	# }	
	# 
	# the base_error_probability by bases qscores at each position
	# at the end, this value will be divided by depth to get the mean qscore a the location
	# the the mean qscore will be used to estimate the noise at the location

	# return the pileup
	return pileup




#
# THIS FUNCTION ALLOWS TO ADD TRUE VARIANTS TO THE READS
#
# INPUT : -PILEUP			: (DICT) THE DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
#		 -READS			 : (DICT) A DICTIONNARY CONTAINING ALL OF R1 AND R2 REFERENCE READS
# 		  -VARIANTS		  : (LIST) THE LIST OF VARIANTS TO BE ADDED TO THE READS
#		 -USEDUMIS		  : (LIST) A LIST OF ALL UMIS THAT ARE ALREADY USED ADN PRESENT AS REFERENCE UMIS
# 		  -BED			   : (STR) LOCATION OF THE BED FILE
#		 -DEPTH			 : (INT) DESIRED DEPTH OF THE FINAL SAMPLE
#		 -AMP_FACTOR		: (INT) THE AMPLFICATION FACTOR OF EACH UMI 
#		 -UMI_LENGTH		 : (INT) DESIRED LENGTH OF THE UMI SEQUENCE
#		 -READ_LENGTH	   : (INT) LENGTH OF THE GENERATED READS
#
# VALUE : -PILEUP			: (DICT) THE UPDATED DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
#  	 	  -INSERTIONS		: (DICT) DICTIONNARY CONTAINING ALL THE READS HAVING A SPECIFIC INSERTION
#		 -DELETIONS		 : (DICT) DICTIONNARY CONTAINING ALL THE READS HAVING A SPECIFIC DELETION


def AddVariantsToReads(pileup, reads, variants, usedUMIS, BED, DEPTH, UMI_LENGTH, READ_LENGTH):

	print("\n")
	PrintTime('console', '\tAdding Variants...')

	insertions = {}
	deletions = {}

	currentPos = 1.0
	lastProgress = 0.0
	totalPos = float(len(variants))

	ranges_d = ParseRanges(BED, READ_LENGTH)

	lenToAlpha = {
		"del": {1: "b",2: "d",3: "e",4: "f",5: "h",6: "i",7: "j",8: "k",9: "l",10: "m"}, 
		"in" : {1: "n",2: "o",3: "p",4: "q",5: "r",6: "s",7: "u",8: "v",9: "w",10: "x"}
	}

	for index, infos in variants.items():

		# print index

		lastProgress = PrintProgress(currentPos, totalPos, lastProgress)

		index = index.split(':')
		chrom = index[0]
		pos = int(index[1])

		freq = infos[1]
		
		typee = infos[2]
		if typee == "sub": 
			alt = infos[0]
		elif typee == "in":
			ref = infos[0][0]
			alt = infos[0][1:]
			insertions[chrom+":"+str(pos)] = [ref, alt]
			alt = lenToAlpha[typee][len(alt)]
		else:
			ref = infos[0][1:]
			alt = infos[0][0]
			deletions[chrom+":"+str(pos)] = [ref, alt]
			alt = lenToAlpha[typee][len(ref)]
		
		varRange = GetRange(ranges_d[chrom], pos)
		var_reads = int(round(freq*DEPTH/2, 0))

		start = varRange[0]
		end   = varRange[1]
		cursor = pos - start

		INDEX = chrom+"-"+str(start)+"-"+str(end)

		readsIDS1 = reads['R1'][INDEX].keys()
		readsIDS2 = reads['R2'][INDEX].keys()

		while var_reads > 0:

			raw = True
			while raw == True:

				chosenKey1 = random.choice(list(readsIDS1))
				chosenKey2 = chosenKey1.replace("1:N:0:1", "2:N:0:1")
				
				chosenRead1 = list(reads['R1'][INDEX][chosenKey1])
				chosenRead2 = list(reads['R2'][INDEX][chosenKey2])


				if chosenRead1[cursor].isupper() and chosenRead2[cursor].isupper():

					chosenRead1[cursor] = alt.lower()
					chosenRead2[cursor] = alt.lower()

					new_umi = False
					while new_umi == False:
						umi = GenerateUMI(UMI_LENGTH)
						if umi not in usedUMIS:

							old_umi = chosenKey1.split(' ')[0].split('_')[1]
							newKey1 = chosenKey1.replace(old_umi, umi)
							newKey2 = chosenKey2.replace(old_umi, umi)

							del reads['R1'][INDEX][chosenKey1]
							del reads['R2'][INDEX][chosenKey2]

							reads['R1'][INDEX][newKey1] = "".join(chosenRead1)
							reads['R2'][INDEX][newKey2] = "".join(chosenRead2)

							readsIDS1 = reads['R1'][INDEX].keys()
							readsIDS2 = reads['R2'][INDEX].keys()
							
							usedUMIS.add(umi)
							new_umi = True


					raw = False
					var_reads -= 1


		currentPos += 1.0


		

	print("\n")
	PrintTime('console', '\tDone')

	return [reads, insertions, deletions]


# THIS FUNCTION ALLOWS TO PARSE A CIGAR SEGMENT CONTAINING ONLY INSERETD BASES AND INCREMENT
# A SPECIFIC PILEUP DICTIONNARY ACCORDINGLY
#
# INPUT : -PILEUP			: (DICT) THE DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
# 		  -UMI			   : (STR)  UMI SEQUENCE OF THE READ
# 		  -CHROM			 : (STR)  THE CHROMOSOME MAPPED TO THE READ
# 		  -POSITION		  : (INT)  THE POSITION OF THE INSERTION SITE IN THE READ
# 		  -SEQ			   : (STR)  THE SEQUENCE OF THE READ
# 		  -STRAND			: (INT)  THE STRAND OF THE READ (0 = FORWARD | 1 = REVERSE)
# 		  -CURSOR_POS		: (INT)  THE CURRENT POSITION IN THE READ
# 		  -CURSOR_SEQ		: (INT)  THE CURRENT POSITION IN THE SEQUENCE OF THE READ
# 		  -MAXX			  : (INT)  THE LENGTH OF THE CIGAR 'I' ELEMENT
# 		  -QUAL			  : (STR)  THE QUALITY STRING OF THE READ
# 		  -QUALS			 : (DICT) A DICTIONNARY FOR THE CONVERSION OF THE QUALITIES FROM ASCII TO INT
# 		  -MIN_BASE_QUALITY  : (INT)  MINIMUM QUALITY SCORE OF THE BASE FOR IT TO ADDED TO THE PILEUP DICTIONNARY
#		  -ALL_UMIS		  : (DICT) A DICTIONNARY CONTAINING UMIS INDEXES
#
# VALUE : -POSITION		  : (INT)  THE FINAL POSITION THAT'S BEEN PARSED IN THE READ
#		  -CURSOR_POS		: (INT)  THE CURRENT POSITION IN THE READ
# 		  -CURSOR_SEQ		: (INT)  THE CURRENT POSITION IN THE SEQUENCE OF THE READ

def AddInsertions(pileup, pileup_mini, umi, chrom, position, seq, strand, cursor_pos, cursor_seq, maxx, qual, quals, MIN_BASE_QUALITY, ALL_UMIS):

	umi = ALL_UMIS[umi]
	
	# create an empty string to contain the inserted sequence
	# create a mean qscore for the inserted sequence 
	inserted_seq = ""
	inserted_qscore = 0

	# try because position in read could not be in BED file
	# therefore, the position could not be in the PILEUP dictionnary
	# if position not in BED, skip the read
	try:
		# testing if position + 1 if in the BED file
		# position is the position of the last matched/mismatched position
		# returned by the AddMatches function, therefore the insertion 
		# occurs at position+1
		test = pileup[chrom][position+1]

		# looping through the inserted sequence
		# maxx = length of the "I" cigar element => length of the inserted_sequence
		for i in range(0, maxx):
			# get base at i
			base = seq[cursor_seq]
			# get base qual at i
			baseQual = qual[cursor_seq]
			# convert base qual to base qscore
			base_qscore = quals[baseQual]

			# add the base at i in seq to the inserted sequence
			inserted_seq += base
			# increment the inserted_seq qscore with the quality 
			# of the inserted base
			inserted_qscore += base_qscore
			# advance in the read sequence
			cursor_seq += 1


		# calculate qscore for the inserted sequence corresponding
		# to the mean of the qscores of the bases in the inserted seq
		inserted_qscore = inserted_qscore/len(inserted_seq)


			
		# check if the quality of the inserted sequence >= minimum base quality
		if inserted_qscore >= MIN_BASE_QUALITY:

			# if this insertion was already seen at this location
			try:
				# try to increment the corresponding counter in the PILEUP dictionnary
				pileup[chrom][position+1]['in'][inserted_seq][strand] += 1
				pileup_mini[chrom][position+1]['in'][inserted_seq][strand] += 1
				# add the umi to the corresponding list specific to the inserted sequence
				pileup[chrom][position+1]['in'][inserted_seq][2].append(umi)
				# increment the qScore of this position
				pileup[chrom][position+1]['base_error_probability'] += inserted_qscore
				pileup_mini[chrom][position+1]['base_error_probability'] += inserted_qscore

			# if this is the first time we see this inserted sequence at this position, 
			# an entry should be created with its appropriated structure (2 counters + set)  
			except:
				# create the entry in PILEUP dictionnary
				pileup[chrom][position+1]['in'][inserted_seq] = [0, 0, [umi]]
				pileup_mini[chrom][position+1]['in'][inserted_seq] = [0, 0, []]
				# increment the corresponding counter in the PILEUP dictionnary
				pileup[chrom][position+1]['in'][inserted_seq][strand] += 1
				pileup_mini[chrom][position+1]['in'][inserted_seq][strand] += 1
				# increment the qScore of this position
				pileup[chrom][position+1]['base_error_probability'] += inserted_qscore
				pileup_mini[chrom][position+1]['base_error_probability'] += inserted_qscore


				

	
	# if position not in BED, skip the read
	except:
		pass	

	# return position, cursor_seq and cursor_pos to continue from where we left off
	return [position, cursor_seq, cursor_pos]























def TreatReads(pileup, pileup_mini, SAM, BED, MIN_BASE_QUALITY, MIN_READ_QUALITY, MIN_MAPPING_QUALITY, ALL_UMIS):

	quals_str = "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJ"
	quals = {}


	i=0
	for q in quals_str:
		quals[quals_str[i]] = i
		i += 1



	currentLine = 1.0
	lastProgress = 0.0
	totalLines = GetTotalLines(SAM)


	for line in open(SAM):

		lastProgress = PrintProgress(currentLine, totalLines, lastProgress)

		currentLine += 1


		line = line.split('\t')
		
		try:
			umi = line[0].split('_')[1]
		except:
			continue


		flag = int(line[1])
		strand = int("{0:b}".format(int(flag))[-5])
		firstInPair = bool(int("{0:b}".format(int(flag))[-7]))
		chrom = line[2]
		pos = line[3]
		mapq = int(line[4])
		cigar = line[5]
		seq = line[9]
		qual = line[10]
		

		if ReadIsValid(flag, chrom, pos, umi, cigar, mapq, MIN_MAPPING_QUALITY, qual, quals, MIN_READ_QUALITY):

			AddRead(pileup, pileup_mini, umi, strand, chrom, int(pos), cigar, seq, qual, quals, MIN_BASE_QUALITY, ALL_UMIS)




	return [pileup, pileup_mini]


















# THIS FUNCTION ALLOWS TO CONFIGURE THE PARAMETERS FROM THE COMMAND LINE
# AND PROPERLY PASS THEM TO THE CORRESPONDING FUNCTION CALLS
#
# INPUT : 
# 		  NONE
#
# VALUE : -CONFIG			  : (DICT)  A DICTIONNARY THAT CONTAINS ALL NECESSARY THE PARAMETERS AND THEIR VALUES
#		




def Config():
	
	config={}


	if len(sys.argv) == 1 or "--help" in sys.argv or "-h" in sys.argv:

		if "snv" in sys.argv:
			PrintHelp("snv")
		elif "cnv" in sys.argv:
			PrintHelp("cnv")
		else:
			PrintHelp("general")







	# configure the variant calling tool
	else:

		### just in case script path contains '-' or '--'
		sys.argv[0] = "None"
		
		

		if 'snv' in sys.argv:
			sys.argv.remove('snv')
			snv = True
			cnv = False

			if len(sys.argv) < 3:
				PrintHelp("snv")


		if 'cnv' in sys.argv:
			sys.argv.remove('cnv')
			snv = False
			cnv = True

			if len(sys.argv) < 3:
				PrintHelp("cnv")



		### default values for parameters
		config["min_base_quality"]    = 10
		config["min_read_quality"]    = 20
		config["min_mapping_quality"] = 20
		config["min_variant_umi"]     = 10 #5
		config["strand_bias_method"]  = "default"
		config["output"]              = os.getcwd()
		config['name']                = "sim"
		config["pileup"]              = "None"
		config["max_hp_length"]       = 7
		config["alpha"]               = 0.05

		config["max_noise_rate"]      = 0.05

		config["variants"]            = "None"
		config["depth"]               = 1000
		config["umi_length"]          = 12
		config["read_length"]         = 110

		config['platform']           = "ILLUMINA"



		### required parameters
		# input : path to the bam/sam file
		# fasta  : path to the fasta file
		# bed  : path to the bed file

		required = ['--input | -i', '--fasta | -f', '--bed | -b']
		for param in required:
			missing = False
			for arg in param.split(" | "):
				try:
					test = sys.argv.index(arg)
					missing = False
					break
				except:
					missing = True



			if missing:
				PrintTime('error', "\tThe parameter "+param+" is required but missing !\n\t\t\tExiting...")
				exit()
		


		PrintProgramName()
		PrintTime("console", "\tConfiguring Parameters...")

		# differentiate the '-' in fasta of the '-' for arguments
		try:
			fastaIdx = sys.argv.index("--fasta")

			fastaIdx+=1
			sys.argv[fastaIdx] = sys.argv[fastaIdx].replace("-", ":")
		except:
			try:
				fastaIdx = sys.argv.index("-f")

				fastaIdx+=1
				sys.argv[fastaIdx] = sys.argv[fastaIdx].replace("-", ":")
			except:
				pass




		# differentiate the '-' in bed of the '-' for arguments
		try:
			bedIdx = sys.argv.index("--bed")

			bedIdx+=1
			sys.argv[bedIdx] = sys.argv[bedIdx].replace("-", ":")
		except:
			try:
				bedIdx = sys.argv.index("-b")

				bedIdx+=1
				sys.argv[bedIdx] = sys.argv[bedIdx].replace("-", ":")
			except:
				pass
		



		# differentiate the '-' in input of the '-' for arguments
		try:
			inputIdx = sys.argv.index("--input")

			inputIdx+=1
			sys.argv[inputIdx] = sys.argv[inputIdx].replace("-", ":")
		except:
			try:
				inputIdx = sys.argv.index("-i")

				inputIdx+=1
				sys.argv[inputIdx] = sys.argv[inputIdx].replace("-", ":")
			except:
				pass





		# differentiate the '-' in output of the '-' for arguments
		try:
			outputIdx = sys.argv.index("--output")
		
			outputIdx+=1
			sys.argv[outputIdx] = sys.argv[outputIdx].replace("-", ":")
		except:
			try:
				outputIdx = sys.argv.index("-o")

				outputIdx+=1
				sys.argv[outputIdx] = sys.argv[outputIdx].replace("-", ":")
			except:
				pass



		# differentiate the '-' in pileup of the '-' for arguments
		try:
			pileupIdx = sys.argv.index("--pileup")

			pileupIdx+=1
			sys.argv[pileupIdx] = sys.argv[pileupIdx].replace("-", ":")
		except:
			try:
				pileupIdx = sys.argv.index("-p")

				pileupIdx+=1
				sys.argv[pileupIdx] = sys.argv[pileupIdx].replace("-", ":")
			except:
				pass






		### verify that no arguments are empty
		params_in_config = ['input', 'bed', 'fasta', 'min_base_quality', 'amp_factor', 'min_read_quality', 'min_mapping_quality', 'min_variant_umi', 'strand_bias_method', 'max_strand_bias', 'output', 'pileup', 'variants', 'alpha', 'max_hp_length', 'depth', 'umi_length', 'read_length', 'max_noise_rate', 'name', 'platform']
		params_mini = ['i', 'b', 'f', 'x', 'x', 'x', 'x', 'x', 'x', 'o', 'p', 'v', 'x', 'x', 'd', 'x', 'u', 'x', 'x', "n", "x"]
		params = ['--input | -i', '--bed | -b', '--fasta | -f', '--min_base_quality', '--amp_factor', '--min_read_quality', '--min_mapping_quality', '--min_variant_umi', '--strand_bias_method', '--max_strand_bias', '--output | -o', '--pileup | -p', '--variants | -v', '--alpha' , '--max_hp_length', '--depth | -d', '--umi_length | -u', '--read_length', '--max_noise_rate', '--name | -n', '--platform']

		pointer = 1
		while pointer < len(sys.argv):
			param = sys.argv[pointer]
			try:
				value = sys.argv[pointer+1]
			except:
				if "--" in param:
					if param in ['--input', '--bed', '--fasta']:
						PrintTime('error', "\tThe parameter "+params[params_in_config.index(param.replace("--", ""))]+" is required and cannot be empty !\n\t\t\tExiting...")
						exit()
					else:
						if param.replace("--", "") in params_in_config:
							PrintTime('error', "\tThe parameter's "+params[params_in_config.index(param.replace("--", ""))]+" value cannot be empty !\n\t\t\tExiting...")
							exit()
						else:
							PrintTime('error', "\tThe parameter "+param+" is unknown !\n\t\t\tExiting...")
							exit()
				elif "-" in param and "--" not in param:
					if param in ['-i', '-b', '-f']:
						PrintTime('error', "\tThe parameter "+params[params_mini.index(param.replace("-", ""))]+" is required and cannot be empty !\n\t\t\tExiting...")
						exit()
					else:
						if param.replace("-", "") in params_mini:
							PrintTime('error', "\tThe parameter's "+params[params_mini.index(param.replace("-", ""))]+" value cannot be empty !\n\t\t\tExiting...")
							exit()
						else:
							PrintTime('error', "\tThe parameter "+param+" is unknown !\n\t\t\tExiting...")
							exit()
				else:
					PrintTime('error', "\tThe parameter "+param+" is unknown !\n\t\t\tExiting...")
					exit()


			if "-" in value and len(value) == 2:
				PrintTime('error', "\tThe parameter "+param+" is required and cannot be empty !\n\t\t\tExiting...")
				exit()

			if "--" in value and len(value) > 3:
				PrintTime('error', "\tThe parameter "+param+" is required and cannot be empty !\n\t\t\tExiting...")
				exit()

			pointer += 2





		args=""
		for arg in sys.argv:
			args+=" "+arg


		args = args.replace("--", "|")
		args = args.replace("-", "|")

		args = args.split("|")
		del args[0]


		sb_set = False

		for arg in args:
			param = arg.split(" ")[0]
			value = arg.split(" ")[1]

			if param == "input" or param == "i":
				param = "input"
				config[param]=[]

				for val in value.split(','):
					if val[0] != "/":
						val = os.getcwd()+"/"+val.replace(":", "-")
					config[param].append(val.replace(":", "-"))

			if param == "fasta" or param == "f":
				param = "fasta"
				if value[0] != "/":
					value = os.getcwd()+"/"+value.replace(":", "-")
				config[param]=value.replace(":", "-")

			if param == "bed" or param == "b":
				param = "bed"
				if value[0] != "/":
					value = os.getcwd()+"/"+value.replace(":", "-")
				config[param]=value.replace(":", "-")


			if param == "pileup" or param == "p":
				param = "pileup"
				if value[0] != "/":
					value = os.getcwd()+"/"+value.replace(":", "-")
				config[param]=value.replace(":", "-")


			if param == "variants" or param == "v":
				param = "variants"
				if value[0] != "/":
					value = os.getcwd()+"/"+value.replace(":", "-")
				config[param]=value.replace(":", "-")


			if param == "output" or param == "o":
				param = "output"
				if value[0] != "/":
					value = os.getcwd()+"/"+value.replace(":", "-")
				config[param]=value.replace(":", "-")



			if param == "min_base_quality":
				try:
					config[param]=float(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a float or an integer !\n\t\t\tExiting...")
					exit()

			if param == "min_read_quality":
				try:
					config[param]=float(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a float or an integer !\n\t\t\tExiting...")
					exit()

			if param == "min_mapping_quality":
				try:
					config[param]=float(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a float or an integer !\n\t\t\tExiting...")
					exit()

			if param == "min_variant_umi":
				try:
					config[param]=float(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a float or an integer !\n\t\t\tExiting...")
					exit()

			if param == "strand_bias_method":
				value = str(value).replace("'", '').replace('"', "")
				try:
					test = value.index('default')
					config[param]=value
				except:
					try:
						test = value.index('torrent_suite')
						config[param]=value
					except:
						PrintTime('error', "\tThe parameter --"+param+" can only be set to \"default\" or \"torrent_suite\" !\n\t\t\tExiting...")
						exit()		

			if param == "max_strand_bias":
				try:
					config[param]=float(value)
					sb_set = True
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a float or an integer !\n\t\t\tExiting...")
					exit()

			if param == "alpha":
				try:
					config[param]=float(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a float or an integer !\n\t\t\tExiting...")
					exit()

			if param == "max_hp_length":
				try:
					config[param]=float(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a float or an integer !\n\t\t\tExiting...")
					exit()


			if param == "amp_factor":
				try:
					config[param]=float(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a float or an integer !\n\t\t\tExiting...")
					exit()



			if param == "depth" or param == "d":
				param = "depth"
				try:
					value = int(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value can only be 100, 1000, 10 000 or 100 000!\n\t\t\tExiting...")
					exit()


				if value >= 1000 and value <= 100000:
					config[param]=int(value)
				else:
					PrintTime('error', "\tThe parameter's --"+param+" value can only be 100, 500, 1000, 2000, 3000, 5000, 10000, 20000, 50000 or 100 000!\n\t\t\tExiting...")
					exit()




			if param == "umi_length" or param == "u":
				param = "umi_length"
				try:
					value = int(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be an 6 <= integer <= 15 !\n\t\t\tExiting...")
					exit()

				if value >= 6 and value <= 15:
					config[param]=int(value)
				else:
					PrintTime('error', "\tThe parameter's --"+param+" value should be an 6 <= integer <= 15 !\n\t\t\tExiting...")
					exit()


			if param == "read_length":
				try:
					value = int(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be an 80 <= integer <= 150 !\n\t\t\tExiting...")
					exit()

				if value >= 80 and value <= 150:
					config[param]=int(value)
				else:
					PrintTime('error', "\tThe parameter's --"+param+" value should be an 80 <= integer <= 150 !\n\t\t\tExiting...")
					exit()


			if param == "max_noise_rate":
				try:
					value = float(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a float <= 1 !\n\t\t\tExiting...")
					exit()

				if value >= 0 and value <= 1:
					config[param]=float(value)
				else:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a float <= 1 !\n\t\t\tExiting...")
					exit()


			if param == "name" or param == "n":
				param = "name"
				try:
					config[param]=str(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be a string !\n\t\t\tExiting...")
					exit()


			if param == "platform":
				values = ['CAPILLARY', 'LS454', 'ILLUMINA', 'SOLID', 'HELICOS', 'IONTORRENT', 'ONT', 'PACBIO']
				try:
					test = values.index(value)
					config[param]=str(value)
				except:
					PrintTime('error', "\tThe parameter's --"+param+" value should be in the list below: \n\t\t\t['CAPILLARY', 'LS454', 'ILLUMINA', 'SOLID', 'HELICOS', 'IONTORRENT', 'ONT', 'PACBIO'] !\n\n\t\t\tExiting...")
					exit()

		if not sb_set:
			if config["strand_bias_method"] == "default":
				config["max_strand_bias"] = 1.0
			else:
				config["max_strand_bias"] = 0.743



		### calculate amp_factor based on depth
		max_unique_umis = 150.0
		min_amp_factor = int(round(config['depth']/2/max_unique_umis))
		if 'amp_factor' in config:
			if float(config['amp_factor']) < min_amp_factor:
				config['amp_factor'] = min_amp_factor
				PrintTime('warning', '\t\tAmplification factor is lower than minimum accepted value! Set to '+str(min_amp_factor))
		else:
			default_unique_umis = max_unique_umis/2
			default_amp_factor = int(round(config['depth']/2/default_unique_umis))
			config['amp_factor'] = default_amp_factor

		return config






# THIS FILE CONTAINS GENERAL FUNCTIONS THAT ARE NEEDED TO 
# SUCCESSFULLY CALL THE VARIANTS IN THE BAM/SAM FILE 


# a class to define colors for each scenario
class bcolors:
	HEADER = '\033[95m'
	OKBLUE = '\033[94m'
	OKGREEN = '\033[92m'
	WARNING = '\033[93m'
	FAIL = '\033[91m'
	ENDC = '\033[0m'
	BOLD = '\033[1m'
	UNDERLINE = '\033[4m'

# a function to print status messages + the time 
# + a specific color for each status
def PrintTime(nature, message, *args):
		
	if nature == "console":
		c_start = bcolors.BOLD+bcolors.OKBLUE
		c_end = bcolors.ENDC
	elif nature == "warning":
		c_start = bcolors.BOLD+bcolors.WARNING
		c_end = bcolors.ENDC
	elif nature == "error":
		c_start = bcolors.BOLD+bcolors.FAIL
		c_end = bcolors.ENDC
	elif nature == "red":
		c_start = bcolors.BOLD+bcolors.FAIL
		c_end = bcolors.ENDC
	elif nature == "green":
		c_start = bcolors.BOLD+bcolors.OKGREEN
		c_end = bcolors.ENDC

	if args:
		message = message % args
		
	print("[ " + time.strftime('%X') + " ]\t"+c_start+ message + c_end)
	sys.stdout.flush()
	sys.stderr.flush()



# function that calculates and displays progress efficiently
def PrintProgress(currentRead, totalReads, lastProgress,):
	progress = int((currentRead/totalReads*100))
	
	if int(progress) > int(lastProgress):
		### write progress in status file
		sys.stdout.write("\r[ " + time.strftime('%X') + " ]\t\t\tWorking "+str(int(progress))+" %")
		sys.stdout.flush()
		# time.sleep(0.001)

	return progress


# function to retieve intial RAM Usage and the exact launching time of the script 
def Start_dev(path):

	startTime = datetime.datetime.now()
	tmp_name = '.'+str(int(time.time()))
	os.mkdir(tmp_name)
	os.system('python '+path+'/RAM.py '+tmp_name+' &')
	return [tmp_name, startTime]



def Exit_dev(tmp_name, startTime):
	
	endTime = datetime.datetime.now()
	totalTime = endTime - startTime

	usedRAM = 0.0
	for x in os.listdir(tmp_name):
		if ".ram" in x:
			usedRAM = float(x[1:-4])
			break

	stop = open(str(tmp_name)+'/.done', 'w')
	stop.close()

	PrintTime('console', "\tDone")

	print('\r\n')
	message = "\tElapsed time :  "+str(totalTime)
	PrintTime("console", message)
	
	try:

		message = "\tRAM USAGE	:  "+str(round(usedRAM, 2))+" GB"+"\n\n"
		PrintTime("console", message)

		time.sleep(1)
		for x in os.listdir(tmp_name):
			os.remove(tmp_name+"/"+x)
		
		os.rmdir(tmp_name)	
	
	except:
		print("\n")

	exit()



def Start(path):

	startTime = datetime.datetime.now()
	tmp_name = '.'+str(int(time.time()))
	return [tmp_name, startTime]



def Exit(tmp_name, startTime):
	
	endTime = datetime.datetime.now()
	totalTime = endTime - startTime

	message = "\t\tElapsed time :  "+str(totalTime)
	PrintTime("green", message)
	PrintTime("console", "\tEND")
	print("\n")


	exit()


# this function will go through the pileup dictionnary and replace sets with lists
# this is because to successfully dump the pileup as a msppack object, sets are not
# supported and should be replaced with basic lists
def RemoveSets(d):
	for chrom , infos in d.items():
		for position, composition in infos.items():
			d[chrom][position]['A'][2] = list(composition['A'][2])
			d[chrom][position]['C'][2] = list(composition['C'][2])
			d[chrom][position]['G'][2] = list(composition['G'][2])
			d[chrom][position]['T'][2] = list(composition['T'][2])
			
			for key, value in composition['in'].items():
				d[chrom][position]['in'][key][2] = list(value[2])
			for key, value in composition['del'].items():
				d[chrom][position]['del'][key][2] = list(value[2])

	return d





# this function takes the pileup dictionnary as an argument
# for each chrom and at each position, it will calculate the 
# depth. the depth = nA+ + nA- + nC+ + nC- + nG+ + nG- + nT+ + nT- + nDel+ + nDel- 
# insertion counts are not taken into account in depth calculation
def AddDepths(d):
	for chrom , infos in d.items():
		for position, composition in infos.items():
			depth =  composition['A'][0]+composition['A'][1]
			depth += composition['C'][0]+composition['C'][1]
			depth += composition['G'][0]+composition['G'][1]
			depth += composition['T'][0]+composition['T'][1]

			n_ins = 0
			for value in composition['in'].values():
				n_ins += value[0]+value[1]
			n_del = 0
			for value in composition['del'].values():
				n_del += value[0]+value[1]

			# insertions shouldn't be taken into account when calculating depth ??
			depth += n_ins
			depth += n_del

			d[chrom][position]['depth'] = depth

	return d



# this function takes the pileup dictionnary as an argument
# for each chrom and at each position, it will calculate use the
# position depth and the total base quality scores to estimate 
# the noise level at the position
def EstimateNoise(d):
	for chrom, infos in d.items():
		for position, composition in infos.items():
			# get total base quality scores at this position
			qScore = d[chrom][position]['base_error_probability']
			
			# get depth
			depth = d[chrom][position]['depth']
			
			n_del = 0
			for value in composition['del'].values():
				n_del += value[0]+value[1]

			# substract deletion from depth because deletions are not taken into account
			# in the total qscore since deletions don't have qscore
			depth -= n_del
			# divide by the depth to get mean qscore of the position
			mean_qscore = math.floor(float(qScore)/depth) if depth > 0 else 0
			d[chrom][position]['qScore'] = mean_qscore

			# qScore = -10.log(base_error_probability)
			# use this formula to calculate base_error probability
			base_error_probability = 10**(float(mean_qscore)/-10)
			# add the estimated base_error_probability to the position in the pileup
			d[chrom][position]['base_error_probability'] = round(base_error_probability, 6)

	return d








# this function takes the pileup dictionnary as an argument
# for each chrom and at each position, it will calculate use the
# position depth and the total base quality scores to estimate 
# the noise level at the position
def CopyNoise(pileup, pileup_mini):
	for chrom, infos in pileup_mini.items():
		for position, composition in infos.items():

			pileup_mini[chrom][position]['qScore'] = pileup[chrom][position]['qScore']

	return pileup_mini







# this function takes the pileup dictionnary as an argument and the refernce genome
# for each chrom and at each position, it will retrieve the reference base in the given
# genome and add it to the pileup dictionnary
def AddRefBases(d, f):
	for chrom , infos in d.items():
		for position, composition in infos.items():
			ref = f[chrom][position-1]
			d[chrom][position]['ref'] = ref.upper()

	return d



# this function generate a random UMI sequence given a specific length
def GenerateUMI(length):
	alphabet = "ACGT"
	return ''.join(random.choice(alphabet) for _ in range(length))



# this fucntion parses the variants file and return a dictionnary
# with every variant infos (SNV)
def ParseVariantsFile(f, ranges_d, depth):
	variants = {}
	removed = []
	all_variants = {}

	for line in open(f):
		if "chr" in line:
			line = line.split(",")
			matchElement = re.match('(chr[A-Z0-9]*:[0-9]*)([A-Z]*)>([A-Z]*).*', line[0])
			variant = matchElement.group(1)
			ref = matchElement.group(2)
			alt = matchElement.group(3)
			freq = float(line[1]) 
			all_variants[variant] = variant+ref+">"+alt

			if freq * depth < 10:
				removed.append(line[0])
			else:
				if len(ref) > len(alt):
					variants[variant] = [ref, freq, "del"]
				elif len(ref) < len(alt):
					variants[variant] = [alt, freq, "in"]
				else:
					variants[variant] = [alt, freq, "sub"]


	mutated_amplicons = []
	toRemove = []

	for variant in variants:
		chrom = variant.split(":")[0]
		pos   = int(variant.split(":")[1])
		ranges = ranges_d[chrom]
		
		i = 0
		for x in ranges:
			if pos in range(x[0], x[1]+1):
				if chrom+"-"+str(i) not in mutated_amplicons:
					mutated_amplicons.append(chrom+"-"+str(i))
				else:
					toRemove.append(all_variants[variant])
			i += 1


	for x in toRemove:
		matchElement = re.match('(chr[A-Z0-9]*:[0-9]*)([A-Z]*)>([A-Z]*).*', x)
		variant = matchElement.group(1)
		del variants[variant]


	return [variants, removed, toRemove]





# this fucntion parses the variations file and return a dictionnary
# with every variation infos (CNV)
def ParseVariationsFile(f):
	variations = {}
	for line in open(f):
		if "type" not in line:
			line = line.split(",")
			chrom = line[0]
			start = line[1]
			end = line[2]
			freq = float(line[3])
			typee = line[4][:-1]

			try:
				variations[chrom][start+"-"+end] = {'type': typee, 'freq': freq}
			except:
				variations[chrom] = {start+"-"+end : {'type': typee, 'freq': freq}}


	return variations



# this function parses the bed file and creates ranges of the covered positions.
# The ranges have the length of the reads so in fact, they correspond to the reads.
def ParseRanges(BED, READ_LENGTH):
	ranges_d = {}

	for line in open(BED):

		# split the line using tab as delimiter
		line = line.split('\t')

		# first element is the chromosome
		chrom = line[0]

		# second element is the start of the region
		# third element is the end of the region
		# create a list containing start and end 
		# sort to ensure that start < end
		limits = [int(line[1]), int(line[2])]
		limits.sort()

		try:
			ranges_d[chrom].append(limits)
		except:
			ranges_d[chrom] = [limits]



	ranges_f = {}

	for chrom in ranges_d.keys():
		ranges_f[chrom] = []

	for chrom in ranges_d.keys():

		c = 0
		nRranges = len(ranges_d[chrom])
		while c < nRranges-1:
			ranges1 = ranges_d[chrom][c]
			ranges2 = ranges_d[chrom][c+1]




			if ranges2[0] <= ranges1[1]:
				newRange = range(ranges1[0], ranges2[1]+1)
				nReads = int(round(len(newRange)/float(READ_LENGTH), 0))
				lenRange = int(round(len(newRange)/2, 0))
				newRange1 = [ranges1[0], ranges1[0]+lenRange]
				newRange2 = [ranges1[0]+lenRange+1, ranges2[1]]


				ranges_d[chrom][c] = newRange1
				ranges_d[chrom][c+1] = newRange2


			c += 1





	return ranges_d



# this function returns the reverse complement of a sequence
def ReverseComplement(read):
	read = read.upper()
	rc = ""
	complement = {"A": "T", "C": "G", "G": "C", "T": "A"}
	for base in read[::-1]:
		rc += complement[base]

	return rc


# this function takes 3 arguments : the quality string of the read, 
# the quals chracters string and the minimum read quality
# if the mean quality of the read is < minimum read quality, the
# function will return False (meaning the read is invalid) 
def QualIsValid(qual, quals, min_read_qual):
	total = 0
	l_read = len(qual)
	threshold = min_read_qual*l_read

	step = 0
	for q in qual:
		total += quals[q]
		step += 1
		if step == 10:
			if total >= threshold:
				return True
			step=0

	return False



# this function will check if the read is valid
# a valid orphan must not be an orphan, must be aligned to chromosome
# at a certain position, must have an UMI of length 12 (customizable),
# must have a read quality > min_read_quality and a mapping quality 
# different than 255. if all these conditions are met, the read is valid
# and the function returns True
def ReadIsValid(flag, chrom, pos, umi, cigar, mapq, min_mapping_qual, qual, quals, min_read_qual):

	orphan = not bool(int("{0:b}".format(int(flag))[-2]))

	# test if orphan
	if orphan:
		return False


	try:
		# test chromosome format
		test = chrom.index('chr')

		# test pos format
		test = 25/int(pos)

		# test umi length
		lUMI = len(umi)
		test = 25/(lUMI-1)
		test = 25/(lUMI-2)
		test = "xx12xx".index(str(len(umi)))
		
		# test cigar format
		test = 25/(1-len(cigar))

		# test read quality
		if not QualIsValid(qual, quals, min_read_qual):
			return False

		# test mapping quality
		if mapq == 255 or mapq < min_mapping_qual:
			return False


		return True

	except:

		return False


# this function will calculate the total number of keys in the given dictionnary
# and return it as a value
def GetPileupLength(pileup):
	total = 0
	
	for chrom , infos in pileup.items():
		for position, composition in infos.items():
			total += 1

	return total


# this function will calculate the total number of variants in the pileup dictionnary
# and return it as a value
def GetTotalVariants(pileup):
	total = 0
	
	for chrom , infos in pileup.items():
		for position, composition in infos.items():
			total += 1
			if composition["alt2"] != None:
				total += 1
			if composition["alt3"] != None:
				total += 1

	return total


# 
# 
def GetTotalPos1(d):
	total = 0

	for chrom, ranges in d.items():
		for aRange in ranges:
			start = aRange[0]
			end   = aRange[1]
			for pos in range(start, end+1):
				total += 1

	return total


#
#
def GetTotalPos2(d):
	total = 0

	for x in d.keys():
		for read_id, read in d[x].items():
			total += 1

	return total



# this function will return the range in which a position occurs
def GetRange(ranges, position):
	for aRange in ranges:
		if position in range(aRange[0], aRange[1]+1):
			return aRange


# this function will return the number of lines in a file
def GetTotalLines(filename):
	with open(filename) as f:
		for i, l in enumerate(f):
			pass
	return i + 1


# this function takes the pileup dictionnary as an argument and the refernce genome
# for each chrom and at each position, it will calculate the length of the homopolymer 
# and add it to the pileup dictionnary
def AddHomoPolymers(d, f):
	for chrom , infos in d.items():
		for position, composition in infos.items():
			ref = f[chrom][position-1]

			hp = 1
			# check hp left - max - 20
			for i in range(2, 22):
				x = f[chrom][position-i]
				if x == ref:
					hp += 1
				else:
					break

			# check hp right - max - 20
			for i in range(0, 20):
				x = f[chrom][position+i]
				if x == ref:
					hp += 1
				else:
					break


			d[chrom][position]['HP'] = hp

	return d



# this function takes the final variants dictionnary as an argument
# it will sort the dictionnary by chromosome first, and by position
# second and return the sorted dictionanry.
def SortVariants(finalVariants):

	tmp = OrderedDict()

	sorted_chroms = sorted(finalVariants.keys())

	chrom_strings = []
	tmp_sorted_chroms = []
	final_sorted_chroms = []
	seen = {}
	
	for sorted_chrom in sorted_chroms:
		try:
			tmp_sorted_chroms.append(int(float(sorted_chrom.replace('chr', '').replace('|v', '.1'))))
		except:
			chrom_strings.append(sorted_chrom)
			
	tmp_sorted_chroms = sorted(tmp_sorted_chroms)
	
	for x in tmp_sorted_chroms:
		if x not in seen:
			final_sorted_chroms.append('chr'+str(x)+"|v")
			seen[x] = 2
		else:
			y = 'chr'+str(x)+"|v"+str(seen[x])
			if y in finalVariants.keys():
				final_sorted_chroms.append('chr'+str(x)+"|v"+str(seen[x]))
				seen[x] += 1
			else:
				seen[x] += 1
				final_sorted_chroms.append('chr'+str(x)+"|v"+str(seen[x]))


	for x in final_sorted_chroms:
		tmp[x] = OrderedDict()

	for x in chrom_strings:
		tmp[x] = OrderedDict()


	for x in tmp.keys():
		subdict = finalVariants[x]
		positions = subdict.keys()
		sorted_positions = sorted(positions)

		for sorted_pos in sorted_positions:
			tmp[x][sorted_pos] = finalVariants[x][sorted_pos]


	return tmp



# this function will return a list with elements that 
# only occured once
def GetSingletons(l):
	d = defaultdict(int)
	for x in l: d[x] += 1
	k = [x for x in l if d[x] == 1]
	return k

# this function will return a list with elements that 
# occured more than once
def RemoveSingletons(l):
	d = defaultdict(int)
	for x in l: d[x] += 1
	k = [x for x in l if d[x] > 1]
	return k


# this function will call pysam to convert the BAM file into a SAM file
def BAMtoSAM(bam):
	sam = bam.replace('.bam', '.sam')
	samFile = open(sam, "w")
	samFile.close()
	pysam.view('-o', sam, bam, save_stdout=sam)
	return sam



# this function will give a variant a confidence level based
# on its q-value, alt_concordant_umi, strand bias and hp length.
def ComputeConfidence(qval, alt_discordant, alt_concordant, hp, Cp, Cm, Vp, Vm):

	sb = CalculateStrandBias(Cp, Cm, Vp, Vm, "default")
	confs = ['low', 'average', 'high', 'strong', 'certain']
	levels = []

	# check q-value
	if qval == 0:
		levels.append(5)
	elif qval < 0.00005:
		levels.append(4)
	elif qval < 0.0005:
		levels.append(3)
	elif qval < 0.005:
		levels.append(2)
	elif qval < 0.01:
		levels.append(1)
	else:
		levels.append(0)



	# check disordant/concordant ratio
	if alt_discordant == 0:
		levels.append(5)
	else:
		if alt_concordant > 30:
			levels.append(5)
		elif alt_concordant > 20:
			levels.append(4)
		elif alt_concordant > 15:
			levels.append(3)
		elif alt_concordant > 10:
			levels.append(2)
		else:
			levels.append(1)


	# check SB
	if sb < 0.5:
		levels.append(5)
	elif sb < 0.60:
		levels.append(4)
	elif sb < 0.70:
		levels.append(3)
	elif sb < 0.80:
		levels.append(2)
	elif sb < 0.90:
		levels.append(1)
	else:
		levels.append(0)

	
	# check HP
	if hp < 3:
		levels.append(5)
	elif hp == 3:
		levels.append(4)
	elif hp == 4:
		levels.append(3)
	elif hp == 5:
		levels.append(2)
	elif hp == 6:
		levels.append(1)
	else:
		levels.append(0)

	conf = int(math.floor(float(sum(levels))/len(levels)))
	return [conf, confs[conf-1]]



# this function will calculate the SB of the variant depending on the method the user selected
def CalculateStrandBias(Cp, Cm, Vp, Vm, method):
	if method == "default":
		return abs(((float(Vp)/(Cp+Vp))-(float(Vm)/(Cm+Vm)))/((float(Vp+Vm))/(Cp+Vp+Cm+Vm)))
	else:
		return float(max([Vp*Cm,Vm*Cp]) / (Vp*Cm + Vm*Cp))



def PrintProgramName():
	print(
"""
||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
|||||||||||||||| ||||||| ||||  ||||||  ||||       ||||||||||||||||    ||||||        ||||  ||||| ||||||||||||||||
|||||||||||||||| ||||||| |||| | |||| | ||||||| |||||||||||||||||| |||| ||||| |||||||||||   |||| ||||||||||||||||
|||||||||||||||| ||||||| |||| || || || ||||||| ||||||||||||||||| ||||||||||| ||||||||||| |  ||| ||||||||||||||||
|||||||||||||||| ||||||| |||| |||  ||| ||||||| |||||||      |||| |||||||||||        |||| ||  || ||||||||||||||||
|||||||||||||||| ||||||| |||| |||||||| ||||||| ||||||||||||||||| ||||   |||| ||||||||||| |||  | ||||||||||||||||
||||||||||||||||  |||||  |||| |||||||| ||||||| |||||||||||||||||| |||| ||||| ||||||||||| ||||   ||||||||||||||||
|||||||||||||||||       ||||| |||||||| ||||       ||||||||||||||||    ||||||        |||| |||||  ||||||||||||||||
||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||V.S|||
||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
""")











# this function will return a dictionnary with all UMIS and their indexes
def GetUMIS(SAM):

	value = set()

	for line in open(SAM):

		line = line.split('\t')
		
		try:
			value.add(line[0].split(':')[6].split('_')[1])

		except:
			continue

	
	c = 0
	final = {}

	for el in value:
		final[el] = c
		c+=1

	return final





def median(data):
	data.sort()
	mid = len(data) // 2
	return (data[mid] + data[~mid]) / 2


#!/usr/bin/python







def SNV(config):
	
	# load and define variables from the config
	samples			  = config['input']
	BED				  = config['bed']
	FASTA				= config['fasta']
	VARIANTS			 = config["variants"]
	MIN_BASE_QUALITY	 = int(config['min_base_quality'])
	MIN_MAPPING_QUALITY  = int(config['min_mapping_quality'])
	MIN_READ_QUALITY	 = int(config['min_read_quality'])
	MIN_VARIANT_UMI	  = int(config['min_variant_umi'])
	STRAND_BIAS_METHOD   = str(config['strand_bias_method'])
	MAX_STRAND_BIAS	  = float(config['max_strand_bias'])
	PILEUP			   = config['pileup']
	REBUILD			  = False if os.path.isfile(PILEUP) else True
	OUTPUT			   = config['output']
	ALPHA				= float(config['alpha'])
	MAX_HP_LENGTH		= int(config['max_hp_length'])
	DEPTH				= int(config['depth'])
	AMP_FACTOR		   = int(config['amp_factor'])
	UMI_LENGTH		   = int(config['umi_length'])
	READ_LENGTH		  = int(config['read_length'])
	MAX_NOISE_RATE	   = float(config['max_noise_rate'])
	OUTPUT_NAME		  = config['name']+"_snv_"+str(DEPTH)


	try:
		os.mkdir(OUTPUT+"/"+OUTPUT_NAME)
	except:
		pass

	OUTPUT = OUTPUT+"/"+OUTPUT_NAME

	# print parameters in the console
	if len(samples) == 1:
		PrintTime("green", "\t\tINPUT file   : "+samples[0])
	else:
		PrintTime("green", "\t\tSAMPLES #	: "+str(len(samples)))

	PrintTime("green", "\t\tBED file	 : "+BED)
	PrintTime("green", "\t\tFASTA file   : "+FASTA)

	if PILEUP != "None":	
		PrintTime("green", "\t\tPILEUP file  : "+PILEUP)
	PrintTime("green", "\t\tOutput	   : "+OUTPUT+"/"+OUTPUT_NAME)

	if VARIANTS != "None":
		PrintTime("green", "\t\tVAR file	 : "+VARIANTS)

	PrintTime("green", "\t\tmin_base_quality	  : "+str(MIN_BASE_QUALITY))
	PrintTime("green", "\t\tmin_read_quality	  : "+str(MIN_READ_QUALITY))
	PrintTime("green", "\t\tmin_mapping_quality   : "+str(MIN_MAPPING_QUALITY))
	PrintTime("green", "\t\tmin_variant_umi	   : "+str(MIN_VARIANT_UMI))
	PrintTime("green", "\t\tstrand_bias_method	: "+str(STRAND_BIAS_METHOD))
	PrintTime("green", "\t\tmax_strand_bias	   : "+str(MAX_STRAND_BIAS))
	PrintTime("green", "\t\tmax_hp_length		 : "+str(MAX_HP_LENGTH))
	PrintTime("green", "\t\talpha				 : "+str(ALPHA))

	PrintTime("green", "\t\tmax_noise_rate		: "+str(MAX_NOISE_RATE))
	PrintTime("green", "\t\tamp_factor			: "+str(AMP_FACTOR))

	PrintTime("green", "\t\tdepth				 : "+str(DEPTH))
	PrintTime("green", "\t\tumi_length			: "+str(UMI_LENGTH))
	PrintTime("green", "\t\tread_length		   : "+str(READ_LENGTH))

	PrintTime("console", "\tDone\n")



	variants = set([])
	pileup = ParseBED(BED)
	ranges_d = ParseRanges(BED, READ_LENGTH)
	f = Fasta(FASTA)


	if PILEUP == "None":

		### create d for qscores at each position
		qScores = {}
		for chrom in pileup.keys():
			for pos in pileup[chrom].keys():
				try:
					qScores[chrom][pos] = []
				except:
					qScores[chrom] = {pos: []}


		for sample in samples:
			value = LaunchVC(pileup, sample, samples.index(sample)+1, config, "snv")
			pileup = value[0]

			for chrom in pileup.keys():
				for pos in pileup[chrom].keys():
					qScores[chrom][pos].append(pileup[chrom][pos]['qScore'])

			for chrom in value[1].keys():
				for pos in value[1][chrom].keys():
					chrom = chrom.split('|')[0]
					variants.add(chrom+"-"+str(pos))



		AddNoiseToPileup(pileup, variants, qScores)


		try:
			os.mkdir('pileups')
		except:
			pass
		
		with open("pileups/snv.pileup", 'wb') as handle:
			msgpack.pack(pileup, handle, encoding="utf-8")

	else:

		print("\n")
		PrintTime('console', "\tLoading Pileup...")

		with open(PILEUP, 'rb') as handle:
			pileup = msgpack.unpack(handle, encoding="utf-8")

		PrintTime('console', "\tDone")





	if VARIANTS == "None":
		variants = {}
		addVariants = False
	else:

		print("\n")
		PrintTime('console', "\tAnalyzing Variants...")

		addVariants = True
		value = ParseVariantsFile(VARIANTS, ranges_d, DEPTH)
		variants = value[0]
		removed_1  = value[1]
		removed_2  = value[2]

		if len(variants) > 0:
			for v in removed_1:
				PrintTime('warning', '\t\tWarning: the variant '+v+' was removed (Not enough depth to add at this frequency)')
			for v in removed_2:
				PrintTime('warning', '\t\tWarning: the variant '+v+' was removed (Another variant is to be added on the same amplicon)')
		else:
			addVariants = False

		PrintTime('console', "\tDone")

	value = GenerateReferenceReads(pileup, variants, BED, DEPTH, AMP_FACTOR, UMI_LENGTH, READ_LENGTH)
	pileup = value[0]
	reads = value[1]
	usedUMIS = value[2]

	value = AddNoiseToReads(pileup, reads, BED, READ_LENGTH)
	pileup = value[0]
	reads = value[1]


	if addVariants:
		value = AddVariantsToReads(pileup, reads, variants, usedUMIS, BED, DEPTH, UMI_LENGTH, READ_LENGTH)
		reads = value[0]
		insertions = value[1]
		deletions = value[2]
	else:
		insertions = {}
		deletions = {}

	Output(reads, FASTA, OUTPUT, OUTPUT_NAME, insertions, deletions)











# THIS FUNCTION ALLOWS TO PARSE A CIGAR SEGMENT CONTAINING ONLY MATCHED/MISMATCHED BASES AND INCREMENT
# A SPECIFIC PILEUP DICTIONNARY ACCORDINGLY
#
# INPUT : -PILEUP			: (DICT) THE DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
# 		  -UMI			   : (STR)  UMI SEQUENCE OF THE READ
# 		  -CHROM			 : (STR)  THE CHROMOSOME MAPPED TO THE READ
# 		  -START			 : (INT)  THE START POSITION OF THE READ
# 		  -SEQ			   : (STR)  THE SEQUENCE OF THE READ
# 		  -STRAND			: (INT)  THE STRAND OF THE READ (0 = FORWARD | 1 = REVERSE)
# 		  -CURSOR_POS		: (INT)  THE CURRENT POSITION IN THE READ
# 		  -CURSOR_SEQ		: (INT)  THE CURRENT POSITION IN THE SEQUENCE OF THE READ
# 		  -MAXX			  : (INT)  THE LENGTH OF THE CIGAR 'M' ELEMENT
# 		  -QUAL			  : (STR)  THE QUALITY STRING OF THE READ
# 		  -QUALS			 : (DICT) A DICTIONNARY FOR THE CONVERSION OF THE QUALITIES FROM ASCII TO INT
# 		  -MIN_BASE_QUALITY  : (INT)  MINIMUM QUALITY SCORE OF THE BASE FOR IT TO ADDED TO THE PILEUP DICTIONNARY
#		  -ALL_UMIS		  : (DICT) A DICTIONNARY CONTAINING UMIS INDEXES
#
# VALUE : -POSITION		  : (INT)  THE FINAL POSITION THAT'S BEEN PARSED IN THE READ
#		  -CURSOR_POS		: (INT)  THE CURRENT POSITION IN THE READ
# 		  -CURSOR_SEQ		: (INT)  THE CURRENT POSITION IN THE SEQUENCE OF THE READ


def AddMatches(pileup, pileup_mini, umi, chrom, start, seq, strand, cursor_pos, cursor_seq, maxx, qual, quals, MIN_BASE_QUALITY, ALL_UMIS):

	umi = ALL_UMIS[umi]

	# for each position between limits
	for position in range(start+cursor_pos, start+cursor_pos+maxx):

		# try because base position in read could not be in BED file
		# therefore, the position could not be in the PILEUP dictionnary
		# if position not in BED, skip the read
		try:	

			# get base, base_qual and base_qscore
			base = seq[cursor_seq]
			baseQual = qual[cursor_seq]
			base_qscore = quals[baseQual]
				
			# check if the quality of the base >= minimum base quality
			if base_qscore >= MIN_BASE_QUALITY:
				# increment the corresponding counter in the PILEUP dictionnary
				pileup[chrom][position][base][strand] += 1
				pileup_mini[chrom][position][base][strand] += 1
				# add the umi to the corresponding list specific to the base in the PILEUP dictionnary
				pileup[chrom][position][base][2].append(umi)
				# increment the qScore of this position
				pileup[chrom][position]['base_error_probability'] += base_qscore
				pileup_mini[chrom][position]['base_error_probability'] += base_qscore

			

		# if position not in BED, skip the read
		except:
			pass	

		# increment the cursor_seq to allow advancing in the read sequence
		cursor_seq += 1


	# increment the cursor_pos with the length of the cigar element when done adding it
	cursor_pos += maxx


	# return position, cursor_seq and cursor_pos to continue from where we left off
	return [position, cursor_seq, cursor_pos]










#
# THIS FUNCTION ALLOWS TO ADD ARTIFACTS AS NOISE TO THE READS
#
# INPUT : -PILEUP			: (DICT) THE DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
# 		  -READS			 : (DICT) A DICTIONNARY CONTAINING ALL OF R1 AND R2 REFERENCE READS
# 		  -BED			   : (STR)  LOCATION OF THE BED FILE
# 		  -READ_LENGTH	   : (INT)  LENGTH OF READS
#
# VALUE : -PILEUP			: (DICT) THE UPDATED DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
#		  -READS			 : (DICT) AN UPDATED VERSION OF THE DICTIONNARY WITH ARTIFACTS ADDED TO THE READS


def AddNoiseToReads(pileup, reads, BED, READ_LENGTH):

	print("\n")
	PrintTime('console', '\tAdding Noise to Reads...')
	
	# build reads extremities for each read depending on the read length so reads don't overlap
	# if regions in the BED file overlap
	ranges_d = ParseRanges(BED, READ_LENGTH)

	currentPos = 1.0
	lastProgress = 0.0
	totalPos = float(GetTotalPos1(ranges_d))


	# for each range
	for chrom, ranges in ranges_d.items():
		# get the start and the end
		for aRange in ranges:
			start = aRange[0]
			end   = aRange[1]

			# build the index
			INDEX = chrom+"-"+str(start)+"-"+str(end)

			# get the reads IDS in R1 and R2
			readsIDS1 = reads['R1'][INDEX].keys()
			readsIDS2 = reads['R2'][INDEX].keys()


			cursor = 0
			# for each position in the read
			for pos in range(start, end+1):

				lastProgress = PrintProgress(currentPos, totalPos, lastProgress)

				# create a list containing only the non reference bases
				notRef = ['A', 'C', 'G', 'T']
				notRef.remove(pileup[chrom][pos]['ref'])


				# for each non ref base
				for x in notRef:

					# get counts for each sens from the pileup
					count_pos = pileup[chrom][pos][x][0]
					count_neg = pileup[chrom][pos][x][1]

					# keep adding forward reads until counts_pos is reached
					while count_pos > 0:
						
						# raw = the read is not modified at the position
						raw = True

						while raw == True:
							# get a radom read
							chosenKey = random.choice(list(readsIDS1))
							chosenRead = list(reads['R1'][INDEX][chosenKey])
							
							# if the base is UPPERCASE at the position, it means that it wasn't changed so we can modify it and raw becomes False
							# else it means that this read was modifed at this position so we skip and choose another one
							if chosenRead[cursor].isupper():
								chosenRead[cursor] = x.lower()

								reads['R1'][INDEX][chosenKey] = "".join(chosenRead)
								
								raw = False
								count_pos -= 1

										
					# we do the same exact thing for R2 reads
					while count_neg > 0:
						
						raw = True

						while raw == True:
							chosenKey = random.choice(list(readsIDS2))
							chosenRead = list(reads['R2'][INDEX][chosenKey])

							if chosenRead[cursor].isupper():
								chosenRead[cursor] = x.lower()

								reads['R2'][INDEX][chosenKey] = "".join(chosenRead)
								
								raw = False
								count_neg -= 1


				# increment the counters
				cursor += 1
				currentPos += 1.0

		

	print("\n")
	PrintTime('console', '\tDone')

	# return pileup and reads
	return [pileup, reads]


# THIS FUNCTION WILL APPLY THE POISSON TEST ON EACH POSITION TO TRY AND FIND A SIGNIFCANT VARIANTS. THEN, P-VALUES ARE CORRECTED
# WITH THE BENJAMINI-HOCHBERG METHOD TO OBTAIN Q-VALUES. ONLY POSITIONS WITH Q-VALUE > ALPHA ARE KEPT
#
# INPUT : 
#		 -PILEUP			  : (DICT)   THE DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
# 		  -ALPHA			   : (FLOAT)  TYPE 1 ERROR PROBABLITY OR ALPHA LEVEL
#
# VALUE : -PILEUP			  : (DICT)   THE DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC



def FilterPositions(pileup, ALPHA):
	print("\n")
	PrintTime('console', "\tSearching for candidate positions...")

	# define counters to calculate progress
	currentPos = 1.0
	lastProgress = 0.0
	totalPos = GetPileupLength(pileup)


	# create two dicts to contain p-values and q-values
	pValues = {}
	qValues = {}

	# create an empty array to stock positions that are not variants
	toRemove = []

	# loop through pileup
	# for each chromosome
	for chrom, infos in pileup.items():
		# for each position | composition = counts
		for position, composition in infos.items():

			# get estimated error probability for this position
			base_error_probability = composition['base_error_probability']

			# function that calculates and displays progress
			lastProgress = PrintProgress(currentPos, totalPos, lastProgress)

			# calculate the estimaded lambda value for the poisson model
			estimated_lambda=int(float(base_error_probability)*composition["depth"])


			# get insertions + and - counts
			n_ins_pos = 0
			n_ins_neg = 0
			for value in composition['in'].values():
				n_ins_pos += value[0]
				n_ins_neg += value[1]
			
			# get deletions + and - counts
			n_del_pos = 0
			n_del_neg = 0
			for value in composition['del'].values():
				n_del_pos += value[0]
				n_del_neg += value[1]
			
			# create a dictionnary with total counts for only base / ins / del counts for this position 
			calls = { 
				"A": composition["A"][0]+composition["A"][1],
				"C": composition["C"][0]+composition["C"][1],
				"G": composition["G"][0]+composition["G"][1],
				"T": composition["T"][0]+composition["T"][1],
				"in": n_ins_pos+n_ins_neg,
				"del": n_del_pos+n_del_neg
			}

			# remove the reference base counts in the dictionnary
			calls.pop(pileup[chrom][position]["ref"], None)	

			# order the remaining alleles by value
			calls = OrderedDict(sorted(calls.items(), key=operator.itemgetter(1), reverse=True))
			
			# check if there is a possible second alternative allele
			pileup[chrom][position]["alt"] = list(calls.keys())[0] if list(calls.values())[0] > 0 else None
			# check if there is a possible second alternative allele
			pileup[chrom][position]["alt2"]= list(calls.keys())[1] if list(calls.values())[1] > 0 else None
			# check if there is a possible third alternative allele
			pileup[chrom][position]["alt3"]= list(calls.keys())[2] if list(calls.values())[2] > 0 else None


			if composition["depth"] <= 0 or pileup[chrom][position]["alt"] == None:
				# increment counter for progress
				currentPos += 1.0
				# if depth == 0 => remove position from pileup (means position is not covered by BAM/SAM file)
				# if the alternative allele count = 0 => no variants at this position
				toRemove.append(chrom+":"+str(position))
				continue

			
			# get the alternative allele count
			n_alt = calls[pileup[chrom][position]["alt"]]

			# calculate allelic frequency of the variant and add it to the PILEUP dictionnary	
			pileup[chrom][position]["VAF"]=float(n_alt)/composition["depth"]
			
			# calculate the pvalue for the variant to be background noise and add it to the PILEUP dictionnary
			pileup[chrom][position]["p-value"] = float(1-poisson.cdf(n_alt, estimated_lambda))
			
			# add the index and the p value to the pvalues dict 
			pValues[chrom+":"+str(position)+"-"] = pileup[chrom][position]["p-value"]

			if pileup[chrom][position]["alt2"] != None:
				# get the second alternative allele count
				n_alt = calls[pileup[chrom][position]["alt2"]]

				# calculate allelic frequency of the variant and add it to the PILEUP dictionnary	
				pileup[chrom][position]["VAF2"]=float(n_alt)/composition["depth"]
				
				# calculate the pvalue for the variant to be background noise and add it to the PILEUP dictionnary
				pileup[chrom][position]["p-value2"] = float(1-poisson.cdf(n_alt, estimated_lambda))
				
				# append the p value to the pvalues list 
				# pValues.append(pileup[chrom][position]["p-value2"])
				pValues[chrom+":"+str(position)+"-2"] = pileup[chrom][position]["p-value2"]



			if pileup[chrom][position]["alt3"] != None:
				# get the third alternative allele count
				n_alt = calls[pileup[chrom][position]["alt3"]]

				# calculate allelic frequency of the variant and add it to the PILEUP dictionnary	
				pileup[chrom][position]["VAF3"]=float(n_alt)/composition["depth"]
				
				# calculate the pvalue for the variant to be background noise and add it to the PILEUP dictionnary
				pileup[chrom][position]["p-value3"] = float(1-poisson.cdf(n_alt, estimated_lambda))
				
				# append the p value to the pvalues list 
				# pValues.append(pileup[chrom][position]["p-value3"])
				pValues[chrom+":"+str(position)+"-3"] = pileup[chrom][position]["p-value3"]



			currentPos += 1.0






	# remove unwanted positions
	for index in toRemove:
		chrom = index.split(":")[0]
		position = int(index.split(":")[1])

		pileup[chrom].pop(position, None)




	# reset toRemove array
	toRemove = []

	# recalculate pileup length
	totalPos = GetPileupLength(pileup)


	# apply FDR Correction (Benjamini/Hochberg Correction) to correct pvalues 
	# based on multiple test theory
	corrected = smm.multipletests(list(pValues.values()), alpha=0.05, method='fdr_bh')

	# retrieve results of the correction
	# retieve results just in case we would need it
	results = corrected[0]
	qValues_list = corrected[1]

	# retrieve qValues
	counter = 0
	for index in pValues.keys():
		qValues[index] = qValues_list[counter]
		counter += 1


	# loop the the qValues dict and the each qValues
	# to its corresponding position in the pileup
	for index, qvalue in qValues.items():
		index = index.split(":")
		chrom = index[0]
		position = int(index[1].split("-")[0])
		alt_index = index[1].split("-")[1]

		pileup[chrom][position]['q-value'+alt_index] = qvalue




	# loop through pileup
	# for each chromosome
	for chrom, infos in pileup.items():
		# for each position | composition = counts		
		for position, composition in infos.items():

			# if qvalue >= alpha(default = 0.05) => consider as artifact
			if pileup[chrom][position]["q-value"] >= ALPHA:
				# remove position from pileup dictionnary
				toRemove.append(chrom+":"+str(position))
			else:
				# if there is a possible second allele variant
				if composition["alt2"] != None:
					# if qvalue2 >= alpha(default = 0.05) => consider as artifact
					if pileup[chrom][position]["q-value2"] >= ALPHA:
						pileup[chrom][position]["alt2"] = None


				# if there is a possible third allele variant
				if composition["alt3"] != None:
					# if qvalue2 >= alpha(default = 0.05) => consider as artifact
					if pileup[chrom][position]["q-value3"] >= ALPHA:
						pileup[chrom][position]["alt3"] = None





	# remove unwanted positions
	for index in toRemove:
		chrom = index.split(":")[0]
		position = int(index.split(":")[1])

		pileup[chrom].pop(position, None)



	# get total kept positions
	potential = GetTotalVariants(pileup)


	print("\n")
	PrintTime('console', "\tDone")


	# return PILEUP dictionnary
	return [pileup, potential]

#
# THIS FUNCTION ALLOWS TO LAUNCH UMI-VARCAL VARRIANT CALLER IN ORDER TO DETECT VARIANTS IN CONTROL SAMPLES
#
# INPUT : -PILEUP_MINI	   : (DICT) THE UPDATED MINI DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC FOR THE CONTROL SAMPLES
#		 -SAMPLE			: (DICT) NAME OF THE SAMPLE BEING ANALYZED
#		 -SAMPLE_NUM		: (DICT) NUMBER OF THE SAMPLE BEING ANALYZED
#		 -CONFIG			: (DICT) DICTIONNARY CONTAINING THE PARAMETERS OF THE VARIANT CALLING ANALYSIS
#
# VALUE : -PILEUP_MINI	   : (DICT) THE UPDATED MINI DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC FOR THE CONTROL SAMPLES
#  	 	  -FINALVARIANTS	 : (DICT) DICTIONNARY CONTAINING ALL THE VARIANTS FOUND IN THE CONTROL SAMPLES




def LaunchVC(pileup_mini, sample, sample_num, config, sim_type):

	# load and define variables from the config
	BED				  = config['bed']
	FASTA				= config['fasta']
	MIN_BASE_QUALITY	 = int(config['min_base_quality'])
	MIN_MAPPING_QUALITY  = int(config['min_mapping_quality'])
	MIN_READ_QUALITY	 = int(config['min_read_quality'])
	MIN_VARIANT_UMI	  = int(config['min_variant_umi'])
	STRAND_BIAS_METHOD   = str(config['strand_bias_method'])
	MAX_STRAND_BIAS	  = float(config['max_strand_bias'])
	PILEUP			   = config['pileup']
	REBUILD			  = False if os.path.isfile(PILEUP) else True
	OUTPUT			   = config['output']
	ALPHA				= float(config['alpha'])
	MAX_HP_LENGTH		= int(config['max_hp_length'])
	MAX_NOISE_RATE	   = float(config["max_noise_rate"])



	# load the reference genome file
	f = Fasta(FASTA)



	# if input is bam => launch samtools view command
	# to convert it to sam
	if ".bam" in sample and ".sam" not in sample:

		print("\n")
		PrintTime('console', "\tConverting BAM to SAM...")

		SAM = BAMtoSAM(sample)

		PrintTime('console', "\tDone")

	else:
		# else => sam = input
		SAM = sample




	# get total number of reads
	totalLines = GetTotalLines(SAM)



	pileup = ParseBED(BED)




	print("\n")
	PrintTime('console', "\tBuilding Pileup for sample #"+str(sample_num)+" ("+SAM.split('/')[-1].replace('.sam', '')+")...")
 	



	# get all UMI list
	ALL_UMIS = GetUMIS(SAM)




	# if only one core is to used, launch the function from here since no need to merge
	value = TreatReads(pileup, pileup_mini, SAM, BED, MIN_BASE_QUALITY, MIN_READ_QUALITY, MIN_MAPPING_QUALITY, ALL_UMIS)
	pileup = value[0]
	pileup_mini = value[1]



	# add depth to pileup
	pileup = AddDepths(pileup)
	pileup_mini = AddDepths(pileup_mini)

	# add variant error noise ate each position
	pileup = EstimateNoise(pileup)
	pileup_mini = CopyNoise(pileup, pileup_mini)

	# add reference bases in the dictionnary
	pileup = AddRefBases(pileup, f)
	pileup_mini = AddRefBases(pileup_mini, f)

	# add homopolymers infos
	pileup = AddHomoPolymers(pileup, f)
	pileup_mini = AddHomoPolymers(pileup_mini, f)



	print("\n")
	PrintTime('console', "\tDone")



	finalVariants = None
	if sim_type == "snv":

		### Poisson modeling to filter positions
		result = FilterPositions(pileup, ALPHA)
		pileup = result[0]
		potential = result[1]



		### call final variants
		finalVariants = CallVariants(pileup, f, STRAND_BIAS_METHOD, MAX_STRAND_BIAS, MIN_VARIANT_UMI, MAX_HP_LENGTH)


		### add positions with high VAF to variants list
		for chrom in pileup.keys():
			for position, composition in pileup[chrom].items():
				if pileup[chrom][position]['VAF'] >= MAX_NOISE_RATE:
					finalVariants[chrom+"|v1"] = {position: ["", ""]}
	

	return [pileup_mini, finalVariants]


















































#
# THIS FUNCTION ALLOWS TO ADD ARTIFACTS AS NOISE TO THE PILEUP
#
# INPUT : -PILEUP			: (DICT) THE DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
# 		  -VARIANTS		  : (LIST) THE LIST OF VARIANTS FOUND BY UMI-VARCAL FOUND IN THE CONTROL SAMPLES
# 		  -QSCORES		   : (DICT) A DIXTIONNARY CONTAINING THE LIST OF QUALITY SCORES FOUND FOR EACH POSITION OF THE PILEUP
#
# VALUE : -PILEUP			: (DICT) THE UPDATED DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC


def AddNoiseToPileup(pileup, variants, qScores):
	# loop through different positions of the pileup
	for chrom in pileup.keys():
		for pos in pileup[chrom].keys():
			# calculate median quality score and add it to the pileup
			pileup[chrom][pos]['qScore'] = median(qScores[chrom][pos])
			# calculate the base error probability from the quality score
			base_error_probability = 10**(float(pileup[chrom][pos]['qScore'])/-10)
			# add the base error probability to the pileup
			pileup[chrom][pos]['base_error_probability'] = round(base_error_probability, 6)

			# build the index to see if position is in the list of variants
			index = chrom+"-"+str(pos)

			# if the index is in variants, change to 0 all probabilities and then make the reference at 1 (0.5/0.5)
			if index in variants:
				pileup[chrom][pos]['A'][0] = 0.0  
				pileup[chrom][pos]['A'][1] = 0.0  
				pileup[chrom][pos]['C'][0] = 0.0  
				pileup[chrom][pos]['C'][1] = 0.0  
				pileup[chrom][pos]['G'][0] = 0.0  
				pileup[chrom][pos]['G'][1] = 0.0  
				pileup[chrom][pos]['T'][0] = 0.0  
				pileup[chrom][pos]['T'][1] = 0.0 

				pileup[chrom][pos][pileup[chrom][pos]['ref']][0] = 0.5  
				pileup[chrom][pos][pileup[chrom][pos]['ref']][1] = 0.5 

				pileup[chrom][pos]['del'] = {}
				pileup[chrom][pos]['in'] = {}

			# if not in variants
			else:

				# change the counts of each base to probabilities by dividing by the depth of each position
				depth = float(pileup[chrom][pos]['depth'])
				try:
					pileup[chrom][pos]['A'][0] /= depth  
					pileup[chrom][pos]['A'][1] /= depth  
					pileup[chrom][pos]['C'][0] /= depth  
					pileup[chrom][pos]['C'][1] /= depth  
					pileup[chrom][pos]['G'][0] /= depth  
					pileup[chrom][pos]['G'][1] /= depth  
					pileup[chrom][pos]['T'][0] /= depth  
					pileup[chrom][pos]['T'][1] /= depth  
					pileup[chrom][pos]['del'] = {}
					pileup[chrom][pos]['in'] = {}
				except:
					pass


	# return pileup
	return pileup

#
# THIS FUNCTION ALLOWS TO GENERATE READS WITH REFERENCE BASES ONLY
#
# INPUT : -PILEUP			: (DICT) THE DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
# 		  -VARIANTS		  : (LIST) THE LIST OF VARIANTS FOUND BY UMI-VARCAL FOUND IN THE CONTROL SAMPLES
# 		  -BED			   : (STR) LOCATION OF THE BED FILE
#		 -DEPTH			 : (INT) DESIRED DEPTH OF THE FINAL SAMPLE
#		 -AMP_FACTOR		: (INT) THE AMPLFICATION FACTOR OF EACH UMI 
#		 -UMI_LENGTH		 : (INT) DESIRED LENGTH OF THE UMI SEQUENCE
#		 -READ_LENGTH	   : (INT) LENGTH OF THE GENERATED READS
#
# VALUE : -PILEUP			: (DICT) THE UPDATED DICTIONNARY CONTAINING COUNTERS THAT ARE CHROMOSOME-POSITION-BASE-STRAND SPECIFIC
#  	 	  -READS			 : (DICT) A DICTIONNARY CONTAINING ALL OF R1 AND R2 REFERENCE READS


def GenerateReferenceReads(pileup, variants, BED, DEPTH, AMP_FACTOR, UMI_LENGTH, READ_LENGTH):
	
	# build reads extremities for each read depending on the read length so reads don't overlap
	# if regions in the BED file overlap
	ranges_d = ParseRanges(BED, READ_LENGTH)



	currentPos = 1.0
	lastProgress = 0.0
	totalPos = float(GetTotalPos1(ranges_d))

	quals_str = "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJ"
	quals = {}


	i=0
	for q in quals_str:
		quals[i] = quals_str[i]
		i += 1


	# build the reads dict with R1 reads and R2 reads separated
	reads = {"R1": OrderedDict(), "R2": OrderedDict()}


	read_counter = 0 
	usedUMIS = set([])

	print("\n")
	PrintTime('console', '\tGenerating Reference Reads...')

	# for each range that correspondsd to a generated read
	for chrom, ranges in ranges_d.items():
		# get read start and read end
		for aRange in ranges:
			start = aRange[0]
			end   = aRange[1]

			# add the range to R1 and R2
			try:
				test = reads['R1'][chrom+"-"+str(start)+"-"+str(end)]
			except:
				reads['R1'][chrom+"-"+str(start)+"-"+str(end)] = OrderedDict()
				reads['R2'][chrom+"-"+str(start)+"-"+str(end)] = OrderedDict()


			read = ""
			read_qual = ""

			for pos in range(start, end+1):

				lastProgress = PrintProgress(currentPos, totalPos, lastProgress)

				pileup[chrom][pos]['A'][0] = round(pileup[chrom][pos]['A'][0]*DEPTH, 0)
				pileup[chrom][pos]['A'][1] = round(pileup[chrom][pos]['A'][1]*DEPTH, 0)

				pileup[chrom][pos]['C'][0] = round(pileup[chrom][pos]['C'][0]*DEPTH, 0)
				pileup[chrom][pos]['C'][1] = round(pileup[chrom][pos]['C'][1]*DEPTH, 0)

				pileup[chrom][pos]['G'][0] = round(pileup[chrom][pos]['G'][0]*DEPTH, 0)
				pileup[chrom][pos]['G'][1] = round(pileup[chrom][pos]['G'][1]*DEPTH, 0)

				pileup[chrom][pos]['T'][0] = round(pileup[chrom][pos]['T'][0]*DEPTH, 0)
				pileup[chrom][pos]['T'][1] = round(pileup[chrom][pos]['T'][1]*DEPTH, 0)



				n_umis   = 0
				
				### adapt amp factor and unique umis count depending on 
				### the variant frequency if position if mutated
				if chrom+":"+str(pos) in variants.keys():
					var_freq = variants[chrom+":"+str(pos)][1]
					var_reads = var_freq * DEPTH
					VAR_AMP_FACTOR = var_reads / 10


					if VAR_AMP_FACTOR > AMP_FACTOR:
						POS_AMP_FACTOR = AMP_FACTOR 
					else:
						POS_AMP_FACTOR = VAR_AMP_FACTOR

					# print POS_AMP_FACTOR

				else:
					POS_AMP_FACTOR = AMP_FACTOR




				# add variability so not all positions have exact AMP FACTOR
				# mean = POS_AMP_FACTOR
				# min = 0.85 * POS_AMP_FACTOR
				# max = 1.15 * POS_AMP_FACTOR
				MIN_POS_AMP_FACTOR = 0.95 * POS_AMP_FACTOR
				MAX_POS_AMP_FACTOR = 1.05 * POS_AMP_FACTOR
				POS_AMP_FACTOR = random.choice(numpy.arange(MIN_POS_AMP_FACTOR, MAX_POS_AMP_FACTOR, 0.05))


				max_umis = float(DEPTH)/2/float(POS_AMP_FACTOR)


				while n_umis < max_umis:
					umi = GenerateUMI(UMI_LENGTH)
					
					if umi not in usedUMIS:
						pileup[chrom][pos][pileup[chrom][pos]['ref']][2].append(umi)
						usedUMIS.add(umi)
						n_umis += 1




				read += pileup[chrom][pos]['ref']
				read_qual += quals[int(pileup[chrom][pos]['qScore'])]

				currentPos += 1.0
				



			# umi_pos = 0
			# umi_count = 0
			# for i in range(start, end):
			# 	x = len(pileup[chrom][i][pileup[chrom][i]['ref']][2])
			# 	if x > umi_count:
			# 		umi_pos = i
			# 		umi_count = x
			# 		pos_amp_factor = float(DEPTH)/x

			i = random.choice(range(start, end))
			umi_pos = i
			x = len(pileup[chrom][i][pileup[chrom][i]['ref']][2])
			pos_amp_factor = float(DEPTH)/x


			for umi in pileup[chrom][umi_pos][pileup[chrom][umi_pos]['ref']][2]:
				i = pos_amp_factor

				while i > 0:

					read_id = "@XXXXX:"+"0"*(15-len(str(read_counter)))+str(read_counter)+"_"+umi+" 1:N:0:1"
					reads["R1"][chrom+"-"+str(start)+"-"+str(end)][read_id] = read+"|"+read_qual
					i -= 1

					if i > 0:
						read_id = "@XXXXX:"+"0"*(15-len(str(read_counter)))+str(read_counter)+"_"+umi+" 2:N:0:1"
						reads["R2"][chrom+"-"+str(start)+"-"+str(end)][read_id] = read+"|"+read_qual

						i -= 1
		
					read_counter += 1



	toRemove = []
	for aRange in reads['R1'].keys():
		keys1 = reads['R1'][aRange]
		keys2 = reads['R2'][aRange]

		if len(keys1) > len(keys2):
			R1 = 'R1'; R2 = 'R2'; i1 = '1'; i2 = '2';
		else:
			R1 = 'R2'; R2 = 'R1'; i1 = '2'; i2 = '1';

		for key in reads[R1][aRange].keys():
			new_key = key.replace(' '+i1+':N:0:1', ' '+i2+':N:0:1')
			
			try:
				test = reads[R2][aRange][new_key]
			except:
				index=R1+"|"+aRange+"|"+key
				toRemove.append(index)
				

	for index in toRemove:
		index = index.split("|")
		R1 = index[0]
		aRange = index[1]
		key = index[2]
		del reads[R1][aRange][key]


	print("\n")
	PrintTime('console', '\tDone')


	return [pileup, reads, usedUMIS]




def PrintHelp(tool):

	tools = ["general", "snv", "cnv"]

	messages = []

	message = """
GENERAL DESCRIPTION
===============================================

This software's main purpose is to produce artificial UMI-tagged short reads that resemble the reads generated from a real 
NGS tumor sequencing. UMI-Gen starts by estimating the mean background error noise in the control samples provided by 
the user. Any variation detected in the control samples that has a frequency higher than a max_noise_rate threshold (0.05 
by default) is automatically removed. The error noise is estimated at each position and inserted in the produced reads. 
Finally, the user can provide a variants file in order to introduce single nucleotide variants (SNV) or copy number 
variants (CNV) in the final reads. A standalone tool is designed for each type of variants. UMI-Gen generates the R1 
FASTQ file, the R2 fastq file and both SAM and BAM alignment files.


:Author: Vincent SATER, LITIS EA 4108 / Centre Henri Becquerel, Rouen, France
:Version: 1.0 
:Last update: 11-04-2020
:Tags: Genomics, UMI, Simulator, Reads, Variant Calling, CNV


There are 2 tools available in this software:

	-SNV
	-CNV


To get help on a specific tool, type:

	python umi-gen.py <tool>
	or 
	python umi-gen.py <tool> --help
	or 
	python umi-gen.py <tool> -h

To use a specific tool, type:

	python umi-gen.py <tool> [tool arguments]\n\n"""

	messages.append(message)
	




	message = """
TOOL DESCRIPTION : SNV
===============================================

This tool's main purpose is to produce artificial UMI-tagged short reads that resemble the reads generated from a real 
NGS tumor sequencing. UMI-Gen starts by estimating the mean background error noise in the control samples provided by 
the user. Any variation detected in the control samples that has a frequency higher than a max_noise_rate threshold (0.05 
by default) is automatically removed. The error noise is estimated at each position and inserted in the produced reads. 
Finally, the user can provide a variants file in order to introduce single nucleotide variants (SNV) in the final reads. 
UMI-Gen generates the R1 FASTQ file, the R2 fastq file and both SAM and BAM alignment files.


:Author: Vincent SATER, LITIS EA 4108 / Centre Henri Becquerel, Rouen, France
:Version: 1.0 
:Last update: 11-04-2020
:Tags: Genomics, UMI, Simulator, Reads, Variant Calling, SNV


USAGE
===============================================

python umi-gen.py snv [required_arguments] [optional_arguments]


DEPENDENCIES
===============================================

-bwa
-samtools
-Python modules:    
                    msgpack-python (version 0.5.6)
					pysam
                    pyfasta
                    scipy.stats
                    statsmodels.stats.multitest


REQUIRED ARGUMENTS
===============================================

-f, --fasta=FILE (case sensitive)
	The reference genome in FASTA format with its index present
	in the same directory.
	It is recommended that you enter the FASTA file full path
	For example: /home/my_dir/my_subdir/hg19.fasta

-i, --input=LIST OF FILES (case sensitive)
	The input is a comma separated list of control BAM/SAM files 
	that will be used to estimate the background error rate of the
	sequencer. Using the full path to each file is recommended. 
	For example: /home/my_dir/my_subdir/ind1.sam,/home/my_dir/my_
	subdir/ind2.bam,/home/my_dir/my_subdir/ind3.sam

-b, --bed=FILE (case sensitive)
	The panel design file in a BED format.
	It is recommended that you enter the BED file full path
	For example: /home/my_dir/my_subdir/design.bed


OPTIONAL ARGUMENTS
===============================================

-p, --pileup=FILE (case sensitive)
	The PILEUP file for the control files previously dumped during
	a older analysis with the same BED and FASTA files. This 
	is not the pileup format generated by SAMtools. If specified,
	the tool will attempt te retrieve the counts of each position
	from the PILEUP file instead of rebuilding the pileup from 
	scratch, a process that is relatively fast and can save time.
	It is recommended that you enter the PILEUP file full path.
	For example: /home/my_dir/my_subdir/ind1-2-3-5.pileup

-o, --output=DIR (case sensitive)
	The output directory in which your files will be generated.
	It is recommended that you enter the output dir full path.
	For example: /home/my_dir/my_subdir/out_dir

-n, --name=STR
	This represents that you want to give to the generated sample.
	A new directory will be created with this name in the OUTPUT dir
	and all generated files will be in it.
	The value of this parameter defaults to 'sim'

-v, --variants=FILE (case sensitive)
	The variants file is a normal CSV file that can be used to 
	introduce known mutations in the final reads. This can be 
	specifically handy for variant calling testing purposes. The 
	file must have 2 columns and a header: variant | frequency. The 
	variant annotation must respect the HGVS-nomenclature and the
	second column must contain frequencies and not percentages.
	Substitutions, insertions and deletions are supported. Inserted 
	variants must be on different amplicons (or separated by at least
	1 * READ_LENGTH)
	It is recommended that you enter the variants file full path.
	For example: /home/my_dir/my_subdir/variants.csv

--min_base_quality=INT
	The minimum value for which a base with a certain quality is
	considered as "good". Only bases with a quality >= min_base_quality
	will be considered.
	The value of this parameter defaults to 10 

--min_read_quality=INT
	The minimum value for which a read with a certain mean quality is
	considered as "good". Only reads with a quality >= min_read_quality
	will be considered.
	The value of this parameter defaults to 20 

--min_mapping_quality=INT
	The minimum value for which a read with a certain mapping 
	quality is considered as "good". Only bases with a mapping
	quality >= min_mapping_quality will be considered.
	The value of this parameter defaults to 20

--min_variant_umi=INT
	The minimum count for a variant with a certain unique UMIs 
	count to be called. Only variants with a unique UMI count >= 
	min_variant_umi will be called.
	The value of this parameter defaults to 10

--alpha=INT/FLOAT
	This is the type I error probability known as alpha level.
	Positions with a p-value/q-value >= alpha will be considered as
	noisy and variants in these positions won't be called.
	The value of this parameter defaults to 0.05 (5%)

--strand_bias_method=STR
	This parameter lets you specifiy the method you want to use
	to calculate variant strand bias. The value can only be 
	"default" or "torrent_suite". Choosing any of the methods 
	can have minimal effect on the final variants called and the SB
	value differs: the torrent suite method generates SB values [0.5;1]
	but the default method generates SB values [0;+Inf].
	Obviously, this parameter defaults to the value "default"

--max_strand_bias=INT/FLOAT
	The minimum strand bias value for a variant with a certain  
	negative and positive coverage to be called. Only variants 
	with a strand bias <= max_strand_bias will be called.
	The value of this parameter defaults to 1.0 for the default
	method and to 0.743 for the torrent suite method
	
--max_hp_length=INT
	The maximum homopolymer length for a variant at a certain  
	position in an homopolymer region to be called. Only variants 
	with a homopolymer length <= max_hp_length will be called.
	The value of this parameter defaults to 7

-d, --depth=INT
	The wanted depth of the produced sample. The minimal depth is
	1000 and the maximum depth is fixed at 100 000. This should be 
	taken into account if you wish to introduce variants at very 
	low frequencies. The value of this parameter defaults to 1000

-u, --umi_length=INT
	The length of the generated UMI tags. The minimal UMI length is
	fixed at 6 and the maximal length is fixed at 15.
	The value of this parameter defaults to 12

--read_length=INT
	The length of the generated reads. The minimal read length is
	fixed at 80 and the maximal length is fixed at 150.
	The value of this parameter defaults to 110

--max_noise_rate=FLOAT
	This parameter represents the frequency above which the variants
	from the control samples are removed. It should be betweeen 0 and 1. 
	The value of this parameter defaults to 0.05

--amp_factor=FLOAT
	This parameter represents the average amplification factor of the 
	reads. At any position, depth = amp_factor * 2 * n_unique_umis.
	For the same depth, increasing this parameter will decrease the 
	number of unique UMI tags. The default value of this parameter is 
	calculated as to allow optimal performance and optimal results. 

--platform=STR
	This parameter corresponds to sequencing platform used to create 
	the sample. Accepted values are CAPILLARY, LS454, ILLUMINA, SOLID,
	HELICOS, IONTORRENT, ONT and PACBIO.
	The default value is ILLUMINA.


EXAMPLE
===============================================

python3 umi-gen.py snv -i /home/.../ind1.sam,/home/.../ind3.sam -b /home/.../design.bed -f /home/.../hg19.fa -p /home/.../test.pileup -o /home/.../out -n test -d 1000 -v /home/.../variants.csv -u 9 --read_length 115 --min_base_quality 15 --min_read_quality 25 --min_mapping_quality 40 --min_variant_umi 7 --strand_bias_method default --max_strand_bias 0.8 --alpha 0.01 --max_hp_length 5 --max_noise_rate 0.03 --amp_factor 35 --platform SOLID


OUTPUT
===============================================

1-  A PILEUP file (<INPUT_NAME>.pileup) corresponding to the entire pileup dumped.
    This file is produced in the pileups directory. If the pileups directory is not
    available, it will be created automatically. This file can be used to skip the
    pileup building and load it instead if the analysis was already done.  It is 
    not compatible with the CNV tool.

2-  A FASTQ_R1 file (<OUTPUT_NAME>_R1.fastq) containing all first-pair reads.

3-  A FASTQ_R2 file (<OUTPUT_NAME>_R2.fastq) containing all second-pair reads.

4-  A SAM file (<OUTPUT_NAME>.sam) containing all the aligned paired-end reads
    from the FASTQ_R1 and FASTQ_R2 files. The alignement is done using BWA algorithm.

5-  A BAM file (<OUTPUT_NAME>.bam) that is the compressed binary version of the 
    SAM file generated above.

6-  A BAM index file (<OUTPUT_NAME>.bam.bai) that is a companion file to your BAM file.
    It allows the program reading the BAM file to jump directly to specific parts of the
    file without reading through all of the sequences.\n\n"""


	messages.append(message)

	message = """
TOOL DESCRIPTION : CNV
===============================================

This tool's main purpose is to produce artificial UMI-tagged short reads that resemble the reads generated from a real 
NGS tumor sequencing. UMI-Gen starts by estimating the mean background error noise in the control samples provided by 
the user. Any variation detected in the control samples that has a frequency higher than a max_noise_rate threshold (0.05 
by default) is automatically removed. The error noise is estimated at each position and inserted in the produced reads. 
Finally, the user can provide a variants file in order to introduce copy number variants (CNV) in the final reads. 
UMI-Gen generates the R1 FASTQ file, the R2 fastq file and both SAM and BAM alignment files.


:Author: Vincent SATER, LITIS EA 4108 / Centre Henri Becquerel, Rouen, France
:Version: 1.0 
:Last update: 11-04-2020
:Tags: Genomics, UMI, Simulator, Reads, Variant Calling, CNV


USAGE
===============================================

python umi-gen.py cnv [required_arguments] [optional_arguments]


DEPENDENCIES
===============================================

-bwa
-samtools
-Python modules:   
                    msgpack-python (version 0.5.6)
                    pysam
                    pyfasta
                    scipy.stats
                    statsmodels.stats.multitest


REQUIRED ARGUMENTS
===============================================

-f, --fasta=FILE (case sensitive)
	The reference genome in FASTA format with its index present
	in the same directory.
	It is recommended that you enter the FASTA file full path
	For example: /home/my_dir/my_subdir/hg19.fasta

-i, --input=LIST OF FILES (case sensitive)
	The input is a comma separated list of control BAM/SAM files 
	that will be used to estimate the background error rate of the
	sequencer. Using the full path to each file is recommended. 
	For example: /home/my_dir/my_subdir/ind1.sam,/home/my_dir/my_
	subdir/ind2.bam,/home/my_dir/my_subdir/ind3.sam, 

-b, --bed=FILE (case sensitive)
	The panel design file in a BED format.
	It is recommended that you enter the BED file full path
	For example: /home/my_dir/my_subdir/design.bed


OPTIONAL ARGUMENTS
===============================================

-p, --pileup=FILE (case sensitive)
	The PILEUP file for the control files previously dumped during
	a older analysis with the same BED and FASTA files. This 
	is not the pileup format generated by SAMtools. If specified,
	the tool will attempt te retrieve the counts of each position
	from the PILEUP file instead of rebuilding the pileup from 
	scratch, a process that is relatively fast and can save time.
	It is recommended that you enter the PILEUP file full path.
	For example: /home/my_dir/my_subdir/ind1-2-3-5.pileup

-o, --output=DIR (case sensitive)
	The output directory in which your files will be generated.
	It is recommended that you enter the output dir full path.
	For example: /home/my_dir/my_subdir/out_dir

-n, --name=STR
	This represents that you want to give to the generated sample.
	A new directory will be created with this name in the OUTPUT dir
	and all generated files will be in it.
	The value of this parameter defaults to 'sim'

-v, --variants=FILE (case sensitive)
	The variants file is a normal CSV file that can be used to 
	introduce known CNV alterations in the final reads. This can be 
	specifically handy for CNV calling testing purposes. The file
	must have 5 columns and a header: chr | start | end | freq | type. 
	The fourth column must contain frequencies and not percentages.
	The fifth column must contain one of these 2 values : "amp" to 
	simulate an amplified region or "del" to simulate a deleted region.
	It is recommended that you enter the variants file full path.
	For example: /home/my_dir/my_subdir/variations.csv

--min_base_quality=INT
	The minimum value for which a base with a certain quality is
	considered as "good". Only bases with a quality >= min_base_quality
	will be considered.
	The value of this parameter defaults to 10 

--min_read_quality=INT
	The minimum value for which a read with a certain mean quality is
	considered as "good". Only reads with a quality >= min_read_quality
	will be considered.
	The value of this parameter defaults to 20 

--min_mapping_quality=INT
	The minimum value for which a read with a certain mapping 
	quality is considered as "good". Only bases with a mapping
	quality >= min_mapping_quality will be considered.
	The value of this parameter defaults to 20

--min_variant_umi=INT
	The minimum count for a variant with a certain unique UMIs 
	count to be called. Only variants with a unique UMI count >= 
	min_variant_umi will be called.
	The value of this parameter defaults to 5

--alpha=INT/FLOAT
	This is the type I error probability known as alpha level.
	Positions with a p-value/q-value >= alpha will be considered as
	noisy and variants in these positions won't be called.
	The value of this parameter defaults to 0.05 (5%)

--strand_bias_method=STR
	This parameter lets you specifiy the method you want to use
	to calculate variant strand bias. The value can only be 
	"default" or "torrent_suite". Choosing any of the methods 
	can have minimal effect on the final variants called and the SB
	value differs: the torrent suite method generates SB values [0.5;1]
	but the default method generates SB values [0;+Inf].
	Obviously, this parameter defaults to the value "default"

--max_strand_bias=INT/FLOAT
	The minimum strand bias value for a variant with a certain  
	negative and positive coverage to be called. Only variants 
	with a strand bias <= max_strand_bias will be called.
	The value of this parameter defaults to 1.0 for the default
	method and to 0.743 for the torrent suite method
	
--max_hp_length=INT
	The maximum homopolymer length for a variant at a certain  
	position in an homopolymer region to be called. Only variants 
	with a homopolymer length <= max_hp_length will be called.
	The value of this parameter defaults to 7

-d, --depth=INT
	The wanted depth of the produced sample. Depth can only take 100, 
	500, 1000, 2000, 3000, 5000, 10 000, 20000, 50000 or 100 000 as 
	values. This should be taken into account if you wish to introduce 
	variants at very low frequencies.
	The value of this parameter defaults to 1000

-u, --umi_length=INT
	The length of the generated UMI tags. The minimal UMI length is
	fixed at 6 and the maximal length is fixed at 15.
	The value of this parameter defaults to 12

--read_length=INT
	The length of the generated reads. The minimal read length is
	fixed at 80 and the maximal length is fixed at 150.
	The value of this parameter defaults to 110

--max_noise_rate=FLOAT
	This parameter represents the frequency above which the variants
	from the control samples are removed. It should be betweeen 0 and 1. 
	The value of this parameter defaults to 0.05

--amp_factor=FLOAT
	This parameter represents the average amplification factor of the 
	reads. At any position, depth = amp_factor * 2 * n_unique_umis.
	For the same depth, increasing this parameter will decrease the 
	number of unique UMI tags. The default value of this parameter is 
	calculated as to allow optimal performance and optimal results. 

--platform=STR
	This parameter corresponds to sequencing platform used to create 
	the sample. Accepted values are CAPILLARY, LS454, ILLUMINA, SOLID,
	HELICOS, IONTORRENT, ONT and PACBIO.
	The default value is ILLUMINA.


EXAMPLE
===============================================

python3 umi-gen.py cnv -i /home/.../ind1.sam,/home/.../ind3.sam -b /home/.../design.bed -f /home/.../hg19.fa -p /home/.../test.pileup -o /home/.../out -n test -d 1000 -v /home/.../variations.csv -u 9 --read_length 115 --min_base_quality 15 --min_read_quality 25 --min_mapping_quality 40 --min_variant_umi 7 --strand_bias_method default --max_strand_bias 0.8 --alpha 0.01 --max_hp_length 5 --max_noise_rate 0.03 --amp_factor 35 --platform PACBIO


OUTPUT
===============================================

1-  A PILEUP file (<INPUT_NAME>.pileup) corresponding to the entire pileup dumped.
    This file is produced in the pileups directory. If the pileups directory is not
    available, it will be created automatically. This file can be used to skip the
    pileup building and load it instead if the analysis was already done.  It is 
    not compatible with the SNV tool.

2-  A FASTQ_R1 file (<OUTPUT_NAME>_R1.fastq) containing all first-pair reads.

3-  A FASTQ_R2 file (<OUTPUT_NAME>_R2.fastq) containing all second-pair reads.

4-  A SAM file (<OUTPUT_NAME>.sam) containing all the aligned paired-end reads
    from the FASTQ_R1 and FASTQ_R2 files. The alignement is done using BWA algorithm.

5-  A BAM file (<OUTPUT_NAME>.bam) that is the compressed binary version of the 
    SAM file generated above.

6-  A BAM index file (<OUTPUT_NAME>.bam.bai) that is a companion file to your BAM file.
    It allows the program reading the BAM file to jump directly to specific parts of the
    file without reading through all of the sequences.\n\n"""



	messages.append(message)




	PrintProgramName()
	print(messages[tools.index(tool)])

	exit()



