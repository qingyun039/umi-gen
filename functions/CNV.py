
#!/usr/bin/python
# import os
# import sys


# import local modules
# from ParseBED import *
# from Config import *
# from LaunchVC import *
# from ParseBED import *
# from pyfasta import Fasta
# from AddNoiseToPileup import *
# from GenerateReferenceReads_CNV import *
# from AddNoiseToReads import *
# from AddVariantsToReads import *
# from utils import *
# from Output_CNV import *


from functions import *



def CNV(config):
	
	# load and define variables from the config
	samples              = config['input']
	BED                  = config['bed']
	FASTA                = config['fasta']
	VARIANTS             = config["variants"]
	MIN_BASE_QUALITY     = int(config['min_base_quality'])
	MIN_MAPPING_QUALITY  = int(config['min_mapping_quality'])
	MIN_READ_QUALITY     = int(config['min_read_quality'])
	MIN_VARIANT_UMI      = int(config['min_variant_umi'])
	STRAND_BIAS_METHOD   = str(config['strand_bias_method'])
	MAX_STRAND_BIAS      = float(config['max_strand_bias'])
	PILEUP               = config['pileup']
	REBUILD              = False if os.path.isfile(PILEUP) else True
	OUTPUT               = config['output']
	ALPHA                = float(config['alpha'])
	MAX_HP_LENGTH        = int(config['max_hp_length'])
	DEPTH                = int(config['depth'])
	AMP_FACTOR           = int(config['amp_factor'])
	UMI_LENGTH           = int(config['umi_length'])
	READ_LENGTH          = int(config['read_length'])
	MAX_NOISE_RATE       = float(config['max_noise_rate'])
	OUTPUT_NAME          = config['name']+"_cnv_"+str(DEPTH)
	NAME                 = config['name']
	PLATFORM             = config['platform']


	try:
		os.mkdir(OUTPUT+"/"+OUTPUT_NAME)
	except:
		pass

	OUTPUT = OUTPUT+"/"+OUTPUT_NAME


	# print parameters in the console
	if len(samples) == 1:
		PrintTime("green", "\t\tINPUT file   : "+samples[0])
	else:
		PrintTime("green", "\t\tSAMPLES #    : "+str(len(samples)))

	PrintTime("green", "\t\tBED file     : "+BED)
	PrintTime("green", "\t\tFASTA file   : "+FASTA)

	if PILEUP != "None":	
		PrintTime("green", "\t\tPILEUP file  : "+PILEUP)
	PrintTime("green", "\t\tOutput       : "+OUTPUT+"/"+OUTPUT_NAME)

	if VARIANTS != "None":
		PrintTime("green", "\t\tVAR file     : "+VARIANTS)

	PrintTime("green", "\t\tmin_base_quality      : "+str(MIN_BASE_QUALITY))
	PrintTime("green", "\t\tmin_read_quality      : "+str(MIN_READ_QUALITY))
	PrintTime("green", "\t\tmin_mapping_quality   : "+str(MIN_MAPPING_QUALITY))
	PrintTime("green", "\t\tmin_variant_umi       : "+str(MIN_VARIANT_UMI))
	PrintTime("green", "\t\tstrand_bias_method    : "+str(STRAND_BIAS_METHOD))
	PrintTime("green", "\t\tmax_strand_bias       : "+str(MAX_STRAND_BIAS))
	PrintTime("green", "\t\tmax_hp_length         : "+str(MAX_HP_LENGTH))
	PrintTime("green", "\t\talpha                 : "+str(ALPHA))

	PrintTime("green", "\t\tmax_noise_rate        : "+str(MAX_NOISE_RATE))

	PrintTime("green", "\t\tdepth                 : "+str(DEPTH))
	PrintTime("green", "\t\tamp_factor            : "+str(AMP_FACTOR))
	PrintTime("green", "\t\tumi_length            : "+str(UMI_LENGTH))
	PrintTime("green", "\t\tread_length           : "+str(READ_LENGTH))

	PrintTime("green", "\t\tplatform              : "+str(PLATFORM))



	PrintTime("console", "\tDone\n")



	variants = set([])
	pileup = ParseBED(BED)
	f = Fasta(FASTA)


	if PILEUP == "None":

		### create d for qscores at each position
		qScores = {}
		for chrom in pileup.keys():
			for pos in pileup[chrom].keys():
				try:
					qScores[chrom][pos] = []
				except:
					qScores[chrom] = {pos: []}


		for sample in samples:
			value = LaunchVC(pileup, sample, samples.index(sample)+1, config, "cnv")
			pileup = value[0]

			for chrom in pileup.keys():
				for pos in pileup[chrom].keys():
					qScores[chrom][pos].append(pileup[chrom][pos]['qScore'])

			# for chrom in value[1].keys():
			# 	for pos in value[1][chrom].keys():
			# 		chrom = chrom.split('|')[0]
			# 		variants.add(chrom+"-"+str(pos))


		AddNoiseToPileup(pileup, variants, qScores)


		try:
			os.mkdir('pileups')
		except:
			pass
		
		with open("pileups/mini_csv.pileup", 'wb') as handle:
			msgpack.pack(pileup, handle, encoding="utf-8")

	else:

		print("\n")
		PrintTime('console', "\tLoading Pileup...")

		with open(PILEUP, 'rb') as handle:
			pileup = msgpack.unpack(handle, encoding="utf-8")

		PrintTime('console', "\tDone")




	if VARIANTS == "None":
		variations = {}
		addVariants = False
	else:

		print("\n")
		PrintTime('console', "\tAnalyzing Variants...")

		addVariants = True
		variations = ParseVariationsFile(VARIANTS)


		PrintTime('console', "\tDone")



	value = GenerateReferenceReads_CNV(pileup, variations, BED, DEPTH, AMP_FACTOR, UMI_LENGTH, READ_LENGTH)
	pileup = value[0]
	reads = value[1]
	usedUMIS = value[2]

	
	# value = AddNoiseToReads(pileup, reads, BED, READ_LENGTH)
	# pileup = value[0]
	# reads = value[1]



	Output_CNV(reads, FASTA, OUTPUT, OUTPUT_NAME, NAME, PLATFORM)








