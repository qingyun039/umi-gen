
from utils import *
from collections import OrderedDict
import random
import time
import math

def GenerateReferenceReads_CNV(pileup, variations, BED, INI_DEPTH, AMP_FACTOR, UMI_LENGTH, READ_LENGTH):

	# calculte average depth of the sample so we can calculate depth ratio after 
	sampleDepth = 0
	totalPos = 0
	for chrom in pileup.keys():
		for pos in pileup[chrom].keys():
			sampleDepth += pileup[chrom][pos]['depth']
			totalPos += 1

	sampleDepth = int(round(float(sampleDepth)/float(totalPos), 0))


	
	ranges_d = ParseRanges(BED, READ_LENGTH)


	currentPos = 1.0
	lastProgress = 0.0
	totalPos = float(GetTotalPos1(ranges_d))

	quals_str = "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJ"
	quals = {}


	i=0
	for q in quals_str:
		quals[i] = quals_str[i]
		i += 1



	reads = {"R1": OrderedDict(), "R2": OrderedDict()}


	read_counter = 0 
	usedUMIS = set([])

	print("\n")
	PrintTime('console', '\tGenerating Reads...')



	for chrom, ranges in ranges_d.items():
		

		for aRange in ranges:

			regionInVariation = False 

			start = aRange[0]
			end   = aRange[1]

			if chrom in variations.keys():
				for var_region in variations[chrom]:
					var_start = int(var_region.split('-')[0])
					var_end = int(var_region.split('-')[1])

					# print aRange
					# print str(var_start) +" <= "+ str(start)
					# print str(var_end) +" >= "+ str(start)
					# print (var_start <= start and var_end >= start)
					# print "\n"
					# print str(var_start) +" >= "+ str(start)
					# print str(var_start) +" <= "+ str(end)
					# print (var_start >= start and var_start <= end)
					# print "\n"

					if len(set(range(start, end+1)).intersection(range(var_start, var_end+1))) > 0:
						regionInVariation = True
						var_chrom = chrom
						break




			DEPTH = INI_DEPTH


			if regionInVariation:

				# print(aRange)

				var_type = variations[var_chrom][var_region]['type']
				var_freq = variations[var_chrom][var_region]['freq']
				if var_type == "amp":
					DEPTH = INI_DEPTH * (1+var_freq)
				else:
					DEPTH = INI_DEPTH * (1-var_freq) 

				NEW_DEPTH = False
				# print(DEPTH)

			else:

				# calculate the range average depth in control samples
				totalDepth = 0
				for pos in range(start, end+1):
					totalDepth += pileup[chrom][pos]['depth']

				rangeDepthRatio = totalDepth/(end-start+1)/sampleDepth
				# print(rangeDepthRatio)
				NEW_DEPTH = rangeDepthRatio * INI_DEPTH





			try:
				test = reads['R1'][chrom+"-"+str(start)+"-"+str(end)]
			except:
				reads['R1'][chrom+"-"+str(start)+"-"+str(end)] = OrderedDict()
				reads['R2'][chrom+"-"+str(start)+"-"+str(end)] = OrderedDict()


			read = ""
			read_qual = ""







			for pos in range(start, end+1):




				if NEW_DEPTH:
					max_umis = float(INI_DEPTH)/2/float(AMP_FACTOR)
					DEPTH = NEW_DEPTH
					if DEPTH > INI_DEPTH:
						min_max_umis = math.floor(max_umis)
						max_max_umis = math.ceil(1.05 * max_umis)
						max_umis = random.choice(range(min_max_umis, max_max_umis))
					elif DEPTH < INI_DEPTH:
						min_max_umis = math.floor(0.95 * max_umis)
						max_max_umis = math.ceil(max_umis)
						max_umis = random.choice(range(min_max_umis, max_max_umis))
					# print(DEPTH)
					# print(max_umis)
					# print("\n")
					# time.sleep(0.1)
				else:
					max_umis = float(DEPTH)/2/float(AMP_FACTOR)
					# print("variation")
					# print(DEPTH)
					# print(max_umis)
					# print('\n')
					# time.sleep(1)



				lastProgress = PrintProgress(currentPos, totalPos, lastProgress)

				pileup[chrom][pos]['A'][0] = round(pileup[chrom][pos]['A'][0]*DEPTH, 0)
				pileup[chrom][pos]['A'][1] = round(pileup[chrom][pos]['A'][1]*DEPTH, 0)

				pileup[chrom][pos]['C'][0] = round(pileup[chrom][pos]['C'][0]*DEPTH, 0)
				pileup[chrom][pos]['C'][1] = round(pileup[chrom][pos]['C'][1]*DEPTH, 0)

				pileup[chrom][pos]['G'][0] = round(pileup[chrom][pos]['G'][0]*DEPTH, 0)
				pileup[chrom][pos]['G'][1] = round(pileup[chrom][pos]['G'][1]*DEPTH, 0)

				pileup[chrom][pos]['T'][0] = round(pileup[chrom][pos]['T'][0]*DEPTH, 0)
				pileup[chrom][pos]['T'][1] = round(pileup[chrom][pos]['T'][1]*DEPTH, 0)



				n_umis   = 0





				while n_umis < max_umis:
					umi = GenerateUMI(UMI_LENGTH)
					
					if umi not in usedUMIS:
						pileup[chrom][pos][pileup[chrom][pos]['ref']][2].append(umi)
						usedUMIS.add(umi)
						n_umis += 1




				read += pileup[chrom][pos]['ref']
				read_qual += quals[int(pileup[chrom][pos]['qScore'])]

				currentPos += 1.0
				



			# umi_pos = 0
			# umi_count = 0
			# for i in range(start, end):
			# 	x = len(pileup[chrom][i][pileup[chrom][i]['ref']][2])
			# 	if x > umi_count:
			# 		umi_pos = i
			# 		umi_count = x
			# 		pos_amp_factor = float(DEPTH)/x

			i = random.choice(range(start, end))
			umi_pos = i
			x = len(pileup[chrom][i][pileup[chrom][i]['ref']][2])
			pos_amp_factor = float(DEPTH)/x


			for umi in pileup[chrom][umi_pos][pileup[chrom][umi_pos]['ref']][2]:
				i = pos_amp_factor

				while i > 0:

					read_id = "@XXXXX:"+"0"*(15-len(str(read_counter)))+str(read_counter)+"_"+umi+" 1:N:0:1"
					reads["R1"][chrom+"-"+str(start)+"-"+str(end)][read_id] = read+"|"+read_qual
					i -= 1

					if i > 0:
						read_id = "@XXXXX:"+"0"*(15-len(str(read_counter)))+str(read_counter)+"_"+umi+" 2:N:0:1"
						reads["R2"][chrom+"-"+str(start)+"-"+str(end)][read_id] = read+"|"+read_qual

						i -= 1
		
					read_counter += 1



	toRemove = []
	for aRange in reads['R1'].keys():
		keys1 = reads['R1'][aRange]
		keys2 = reads['R2'][aRange]

		if len(keys1) > len(keys2):
			R1 = 'R1'; R2 = 'R2'; i1 = '1'; i2 = '2';
		else:
			R1 = 'R2'; R2 = 'R1'; i1 = '2'; i2 = '1';

		for key in reads[R1][aRange].keys():
			new_key = key.replace(' '+i1+':N:0:1', ' '+i2+':N:0:1')
			
			try:
				test = reads[R2][aRange][new_key]
			except:
				index=R1+"|"+aRange+"|"+key
				toRemove.append(index)
				

	for index in toRemove:
		index = index.split("|")
		R1 = index[0]
		aRange = index[1]
		key = index[2]
		del reads[R1][aRange][key]


	print("\n")
	PrintTime('console', '\tDone')


	return [pileup, reads, usedUMIS]